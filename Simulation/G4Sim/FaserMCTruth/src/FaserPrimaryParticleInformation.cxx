/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "FaserMCTruth/FaserPrimaryParticleInformation.h"

FaserPrimaryParticleInformation::FaserPrimaryParticleInformation() 
  : m_theParticle(0),m_theISFParticle(0),m_regenerationNr(0),m_barcode(-1)
{
}

FaserPrimaryParticleInformation::FaserPrimaryParticleInformation(const HepMC::GenParticle *p, const ISF::FaserISFParticle* isp):m_theParticle(p),m_theISFParticle(isp),m_regenerationNr(0),m_barcode(-1)
{
}

const HepMC::GenParticle* FaserPrimaryParticleInformation::GetHepMCParticle() const
{
	return m_theParticle;
}

const ISF::FaserISFParticle* FaserPrimaryParticleInformation::GetISFParticle() const
{
	return m_theISFParticle;
}

void FaserPrimaryParticleInformation::SuggestBarcode(int bc)
{
  m_barcode=bc;
  if (m_theParticle) {
    std::cout<<"ERROR: PrimaryParticleInformation::SuggestBarcode() should be only called if no HepMC::Particle is available"<<std::endl;
    //theParticle->suggest_barcode(bc);
  }
}

int FaserPrimaryParticleInformation::GetParticleBarcode() const
{
	return m_theParticle?m_theParticle->barcode():m_barcode;
}

void FaserPrimaryParticleInformation::SetParticle(const HepMC::GenParticle* p)
{
	m_theParticle=p;
}

void FaserPrimaryParticleInformation::SetISFParticle(const ISF::FaserISFParticle* p)
{
	m_theISFParticle=p;
}
