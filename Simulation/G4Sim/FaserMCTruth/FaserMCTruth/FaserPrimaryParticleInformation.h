/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FaserPrimaryParticleInformation_H
#define FaserPrimaryParticleInformation_H

#include "G4VUserPrimaryParticleInformation.hh"
#include "AtlasHepMC/GenEvent.h"

namespace ISF {
  class FaserISFParticle;
}

class FaserPrimaryParticleInformation: public G4VUserPrimaryParticleInformation {
public:
	FaserPrimaryParticleInformation();
	FaserPrimaryParticleInformation(const HepMC::GenParticle*, const ISF::FaserISFParticle* isp=0);
	const HepMC::GenParticle *GetHepMCParticle() const;
	int GetParticleBarcode() const;
	void SuggestBarcode(int bc);
	void SetParticle(const HepMC::GenParticle*);
	void Print() const {}
	int GetRegenerationNr() {return  m_regenerationNr;}
	void SetRegenerationNr(int i) {m_regenerationNr=i;}

	void SetISFParticle(const ISF::FaserISFParticle* isp);
	const ISF::FaserISFParticle* GetISFParticle() const;

private:
	const HepMC::GenParticle *m_theParticle;
	const ISF::FaserISFParticle* m_theISFParticle;

	int m_regenerationNr;
	int m_barcode;
};

#endif
