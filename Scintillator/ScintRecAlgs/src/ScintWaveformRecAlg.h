#ifndef SCINTRECALGS_SCINTWAVEFORMRECALG_H
#define SCINTRECALGS_SCINTWAVEFORMRECALG_H

// Base class
#include "AthenaBaseComps/AthReentrantAlgorithm.h"

// Data classes
#include "ScintRawEvent/ScintWaveformContainer.h"

#include "xAODFaserWaveform/WaveformClock.h"
#include "xAODFaserWaveform/WaveformClockAuxInfo.h"

#include "xAODFaserWaveform/WaveformHit.h"
#include "xAODFaserWaveform/WaveformHitContainer.h"
#include "xAODFaserWaveform/WaveformHitAuxContainer.h"

// Tool classes
#include "ScintRecTools/IWaveformReconstructionTool.h"

// Handles
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"

// Gaudi
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"

// STL
#include <string>

class ScintWaveformRecAlg : public AthReentrantAlgorithm {

 public:
  // Constructor
  ScintWaveformRecAlg(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~ScintWaveformRecAlg() = default;

  /** @name Usual algorithm methods */
  //@{
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& ctx) const override;
  virtual StatusCode finalize() override;
  //@}

 private:

  /** @name Disallow default instantiation, copy, assignment */
  //@{
  ScintWaveformRecAlg() = delete;
  ScintWaveformRecAlg(const ScintWaveformRecAlg&) = delete;
  ScintWaveformRecAlg &operator=(const ScintWaveformRecAlg&) = delete;
  //@}

  /**
   * @name Reconstruction tool
   */
  ToolHandle<IWaveformReconstructionTool> m_recoTool
    {this, "WaveformReconstructionTool", "WaveformReconstructionTool"};

  /**
   * @name Input raw waveform data using SG::ReadHandleKey
   */
  //@{
  SG::ReadHandleKey<ScintWaveformContainer> m_waveformContainerKey
    {this, "WaveformContainerKey", ""};
  //@}

  /**
   * @name Input WaveformClock data using SG::ReadHandleKey
   */
  //@{
  SG::ReadHandleKey<xAOD::WaveformClock> m_clockKey
    {this, "WaveformClockKey", "WaveformClock"};
  //@}

  /**
   * @name Output data using SG::WriteHandleKey
   */
  //@{
  SG::WriteHandleKey<xAOD::WaveformHitContainer> m_waveformHitContainerKey
    {this, "WaveformHitContainerKey", ""};
  //@}

};

#endif // SCINTRECALGS_SCINTWAVEFORMRECALG_H
