#ifndef SCINTRECALGS_SCINTCLOCKRECALG_H
#define SCINTRECALGS_SCINTCLOCKRECALG_H

// Base class
#include "AthenaBaseComps/AthReentrantAlgorithm.h"

// Input data
#include "ScintRawEvent/ScintWaveformContainer.h"

// Output data
// #include "ScintRecEvent/WaveformClock.h"
#include "xAODFaserWaveform/WaveformClock.h"
#include "xAODFaserWaveform/WaveformClockAuxInfo.h"
#include "ScintRecTools/IClockReconstructionTool.h"

#include "StoreGate/ReadHandleKey.h"

// Gaudi
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"

// STL
#include <string>

class ScintClockRecAlg : public AthReentrantAlgorithm {

 public:
  // Constructor
  ScintClockRecAlg(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~ScintClockRecAlg() = default;

  /** @name Usual algorithm methods */
  //@{
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& ctx) const override;
  virtual StatusCode finalize() override;
  //@}

 private:

  /** @name Disallow default instantiation, copy, assignment */
  //@{
  ScintClockRecAlg() = delete;
  ScintClockRecAlg(const ScintClockRecAlg&) = delete;
  ScintClockRecAlg &operator=(const ScintClockRecAlg&) = delete;
  //@}

  /**
   * @name Reconstruction tool
   */
  ToolHandle<IClockReconstructionTool> m_recoTool
    {this, "ClockReconstructionTool", "ClockReconstructionTool"};

  /**
   * @name Input data using SG::ReadHandleKey
   */
  //@{
  SG::ReadHandleKey<ScintWaveformContainer> m_waveformContainerKey
    {this, "WaveformContainerKey", "ClockWaveforms"};
  //@}

  /**
   * @name Output data using SG::WriteHandleKey
   */
  //@{
  SG::WriteHandleKey<xAOD::WaveformClock> m_waveformClockKey
    {this, "WaveformClockKey", "WaveformClock"};
  //@}

};

#endif // SCINTRECALGS_SCINTCLOCKRECALG_H
