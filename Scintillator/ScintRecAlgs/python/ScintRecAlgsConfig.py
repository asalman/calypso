""" Define methods used to instantiate configured Waveform reconstruction tools and algorithms

Copyright (C) 2020 CERN for the benefit of the FASER collaboration
"""
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg

WaveformReconstructionTool = CompFactory.WaveformReconstructionTool
ClockReconstructionTool = CompFactory.ClockReconstructionTool

# One stop shopping for normal FASER data
def WaveformReconstructionCfg(flags):
    """ Return all algorithms and tools for Waveform reconstruction """
    acc = ComponentAccumulator()

    acc.merge(WaveformClockRecCfg(flags, "ClockRecAlg"))

    acc.merge(WaveformHitRecCfg(flags, "VetoWaveformRecAlg", "Veto"))
    acc.merge(WaveformHitRecCfg(flags, "TimingWaveformRecAlg", "Trigger"))
    acc.merge(WaveformHitRecCfg(flags, "PreshowerWaveformRecAlg", "Preshower"))
    acc.merge(WaveformHitRecCfg(flags, "CaloWaveformRecAlg", "Calo"))
    return acc

# Return configured WaveformClock reconstruction algorithm
def WaveformClockRecCfg(flags, name="ClockRecAlg", **kwargs):

    acc = ComponentAccumulator()

    tool = ClockReconstructionTool(name="ClockReconstructionTool")
    # tool.CheckResult = True
    kwargs.setdefault("ClockReconstructionTool", tool)

    recoAlg = CompFactory.ScintClockRecAlg(name, **kwargs)
    recoAlg.ClockReconstructionTool = tool
    acc.addEventAlgo(recoAlg)

    return acc

# Return configured WaveformHit reconstruction algorithm
# Specify data source (Veto, Trigger, Preshower, Calo, Test)
def WaveformHitRecCfg(flags, name="WaveformRecAlg", source="", **kwargs):

    acc = ComponentAccumulator()

    tool = WaveformReconstructionTool(name=source+"WaveformRecTool", **kwargs)
    kwargs.setdefault("WaveformContainerKey", source+"Waveforms")
    kwargs.setdefault("WaveformHitContainerKey", source+"WaveformHits")
    kwargs.setdefault("WaveformReconstructionTool", tool)
              
    recoAlg = CompFactory.ScintWaveformRecAlg(name, **kwargs)
    recoAlg.WaveformReconstructionTool = tool
    acc.addEventAlgo(recoAlg)

    return acc

def WaveformReconstructionOutputCfg(flags, **kwargs):
    """ Return ComponentAccumulator with output for Waveform Reco"""
    acc = ComponentAccumulator()
    ItemList = [
        "xAOD::WaveformHitContainer#*"
        , "xAOD::WaveformHitAuxContainer#*"
        , "xAOD::WaveformClock#*"
        , "xAOD::WaveformClockAuxInfo#*"
    ]
    acc.merge(OutputStreamCfg(flags, "xAOD", ItemList))
    # ostream = acc.getEventAlgo("OutputStreamRDO")
    # ostream.TakeItemsFromInput = True # Don't know what this does
    return acc
