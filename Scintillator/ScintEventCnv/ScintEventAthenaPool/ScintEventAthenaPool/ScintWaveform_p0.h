/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef SCINTWAVEFORM_P0_H
#define SCINTWAVEFORM_P0_H

#include <vector>
#include <iostream>
#include <iomanip>

class ScintWaveform_p0 {
 public:
  ScintWaveform_p0();

// List of Cnv classes that convert this into Rdo objects
  friend class ScintWaveformCnv_p0;

 private:
  unsigned int m_ID;
  unsigned int m_channel;
  std::vector<unsigned int> m_adc_counts;

 public:
  void print() const {
    std::cout << "Persistent Waveform data:" << std::endl
	      << std::setw(30) << " channel:              "<<std::setfill(' ')<<std::setw(32)<<std::dec<<m_channel<<std::setfill(' ')<<std::endl
	      << std::setw(30) << " length:               "<<std::setfill(' ')<<std::setw(32)<<std::dec<<m_adc_counts.size()<<std::setfill(' ')<<std::endl;
  }
};

inline
ScintWaveform_p0::ScintWaveform_p0() : m_channel(0) { 
  m_adc_counts.clear(); 
}

// Stream operator for debugging
//std::ostream
//&operator<<(std::ostream &out, const ScintWaveform_p0 &wfm) {
//  return wfm.print(out);
//}

#endif
