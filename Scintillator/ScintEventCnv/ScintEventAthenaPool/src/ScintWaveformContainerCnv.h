/*
 Copyright 2020 CERN for the benefit of the FASER collaboration
*/

#ifndef SCINTWAVEFORMCONTAINERCNV_H
#define SCINTWAVEFORMCONTAINERCNV_H

#include "AthenaPoolCnvSvc/T_AthenaPoolCustomCnv.h"

#include "ScintWaveformContainerCnv_p0.h"

#include "ScintRawEvent/ScintWaveformContainer.h"
#include "ScintEventAthenaPool/ScintWaveformContainer_p0.h"

// The latest persistent representation
typedef ScintWaveformContainer_p0 ScintWaveformContainer_PERS;
typedef ScintWaveformContainerCnv_p0 ScintWaveformContainerCnv_PERS;

typedef T_AthenaPoolCustomCnv< ScintWaveformContainer, ScintWaveformContainer_PERS > ScintWaveformContainerCnvBase;

class ScintWaveformContainerCnv : public ScintWaveformContainerCnvBase {
  friend class CnvFactory<ScintWaveformContainerCnv>;

 public:
  ScintWaveformContainerCnv (ISvcLocator* svcloc) : ScintWaveformContainerCnvBase(svcloc) {}

 protected:
  virtual ScintWaveformContainer_PERS* createPersistent (ScintWaveformContainer* transCont);
  virtual ScintWaveformContainer* createTransient ();

};

#endif
