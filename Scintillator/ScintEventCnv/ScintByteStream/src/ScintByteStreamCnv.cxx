/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

#include "ScintByteStreamCnv.h"
#include "ScintWaveformDecoderTool.h"

#include "FaserByteStreamCnvSvcBase/IFaserROBDataProviderSvc.h"
#include "ScintRawEvent/ScintWaveform.h"
#include "ScintRawEvent/ScintWaveformContainer.h"
#include "EventFormats/DAQFormats.hpp"

#include "AthenaKernel/errorcheck.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"

#include "StoreGate/StoreGateSvc.h"
#include "GaudiKernel/IToolSvc.h"

using DAQFormats::EventFull;

ScintByteStreamCnv::ScintByteStreamCnv(ISvcLocator* svcloc)
  : Converter(storageType(), classID(), svcloc)
  , AthMessaging(svcloc != nullptr ? msgSvc() : nullptr, "ScintByteStreamCnv")
  , m_name("ScintByteStreamCnv")
  , m_tool("ScintWaveformDecoderTool")
  , m_rdpSvc("FaserROBDataProviderSvc", m_name)
{
  ATH_MSG_DEBUG(m_name+"::initialize() called");
}

ScintByteStreamCnv::~ScintByteStreamCnv() {
}

const CLID& ScintByteStreamCnv::classID() 
{
  // Change to our data object
  return ClassID_traits<ScintWaveformContainer>::ID();
}

StatusCode ScintByteStreamCnv::initialize() 
{
  ATH_MSG_DEBUG(m_name+"::initialize() called");

  CHECK(Converter::initialize());
  CHECK(m_rdpSvc.retrieve());
  CHECK(m_tool.retrieve());

  return StatusCode::SUCCESS;
}

StatusCode ScintByteStreamCnv::finalize() 
{
  ATH_MSG_DEBUG("ScintByteStreamCnv::Finalize");

  CHECK(Converter::finalize());
  return StatusCode::SUCCESS;  

}

StatusCode ScintByteStreamCnv::createObj(IOpaqueAddress* pAddr, DataObject*& pObj) 
{
  ATH_MSG_DEBUG("ScintByteStreamCnv::createObj() called");

  // Check that we can access raw data
  if (!m_rdpSvc) {
    ATH_MSG_ERROR("ScintByteStreamCnv::createObj() - ROBDataProviderSvc not loaded!");
    return StatusCode::FAILURE;
  }

  FaserByteStreamAddress *pRE_Addr{nullptr};
  pRE_Addr = dynamic_cast<FaserByteStreamAddress*>(pAddr); // Cast from OpaqueAddress
  if (!pRE_Addr) {
    ATH_MSG_ERROR("Cannot cast to FaserByteStreamAddress ");
    return StatusCode::FAILURE;
  }

  // Get pointer to the raw event
  const EventFull* re = m_rdpSvc->getEvent();
  if (!re) {
    ATH_MSG_ERROR("Cannot get raw event from FaserByteStreamInputSvc!");
    return StatusCode::FAILURE;
  }               

  // Get key used in the StoreGateSvc::retrieve function
  const std::string key = *(pRE_Addr->par());
  ATH_MSG_DEBUG("ScintByteStreamCnv - creating objects "+key);

  ScintWaveformContainer* wfmCont = new ScintWaveformContainer;

  // Convert selected channels

  CHECK( m_tool->convert(re, wfmCont, key) );
  
  pObj = SG::asStorable(wfmCont);

  ATH_MSG_DEBUG(" New xAOD::ScintWaveformData created");
  return StatusCode::SUCCESS;
}

