// -*- C++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: FaserTruthParticle.cxx 690336 2015-08-20 10:54:57Z abuckley $

// System include(s):
#include <cmath>
#include <iostream>
#include <stdexcept>

// Utility include(s):
#include "TruthUtils/PIDHelpers.h"

// xAOD include(s):
#include "xAODCore/AuxStoreAccessorMacros.h"

// Local include(s):
#include "xAODFaserTruth/FaserTruthParticle.h"
#include "xAODFaserTruth/FaserTruthVertexContainer.h"
#include "FaserTruthAccessors.h"

namespace xAOD {

   FaserTruthParticle::FaserTruthParticle()
   : IParticle() {

   }

   /////////////////////////////////////////////////////////////////////////////
   //
   //  Implementation for functions identifying the particle in the MC record
   //

   AUXSTORE_PRIMITIVE_SETTER_AND_GETTER( FaserTruthParticle, int, pdgId,
                                         setPdgId )

   int FaserTruthParticle::absPdgId() const {

      return std::abs( pdgId() );
   }

   AUXSTORE_PRIMITIVE_SETTER_AND_GETTER( FaserTruthParticle, int, barcode,
                                         setBarcode )
   AUXSTORE_PRIMITIVE_SETTER_AND_GETTER( FaserTruthParticle, int, status,
                                         setStatus )

   //
   /////////////////////////////////////////////////////////////////////////////

   /////////////////////////////////////////////////////////////////////////////
   //
   //                Implementation of the links to the vertices
   //

   /// Accessor for the production vertex
   static SG::AuxElement::Accessor< ElementLink< FaserTruthVertexContainer > >
      prodVtxLinkAcc( "prodVtxLink" );
   /// Accessor for the decay vertex
   static SG::AuxElement::Accessor< ElementLink< FaserTruthVertexContainer > >
      decayVtxLinkAcc( "decayVtxLink" );

   bool FaserTruthParticle::hasProdVtx() const {

      return ( prodVtxLinkAcc.isAvailable( *this ) &&
               prodVtxLinkAcc( *this ).isValid() );
   }

   const FaserTruthVertex* FaserTruthParticle::prodVtx() const {

      return hasProdVtx() ? *prodVtxLink() : 0;
   }

   AUXSTORE_OBJECT_SETTER_AND_GETTER( FaserTruthParticle,
                                      ElementLink< FaserTruthVertexContainer >,
                                      prodVtxLink, setProdVtxLink )

   bool FaserTruthParticle::hasDecayVtx() const {

      return ( decayVtxLinkAcc.isAvailable( *this ) &&
               decayVtxLinkAcc( *this ).isValid() );
   }

   const FaserTruthVertex* FaserTruthParticle::decayVtx() const {

      return hasDecayVtx() ? *decayVtxLink() : 0;
   }

   AUXSTORE_OBJECT_SETTER_AND_GETTER( FaserTruthParticle,
                                      ElementLink< FaserTruthVertexContainer >,
                                      decayVtxLink, setDecayVtxLink )

   //
   /////////////////////////////////////////////////////////////////////////////

   /////////////////////////////////////////////////////////////////////////////
   //
   //                 Direct access to parents and children
   //

   size_t FaserTruthParticle::nParents() const {

      return hasProdVtx() ? prodVtx()->nIncomingParticles() : 0;
   }

   const FaserTruthParticle* FaserTruthParticle::parent( size_t i ) const {

      return hasProdVtx() ? prodVtx()->incomingParticle( i ) : 0;
   }

   size_t FaserTruthParticle::nChildren() const {

      return hasDecayVtx() ? decayVtx()->nOutgoingParticles() : 0;
   }

   const FaserTruthParticle* FaserTruthParticle::child( size_t i ) const {

      return hasDecayVtx() ? decayVtx()->outgoingParticle( i ) : 0;
   }

   //
   /////////////////////////////////////////////////////////////////////////////

   /////////////////////////////////////////////////////////////////////////////
   //
   //               Implementation of the IParticle interface
   //

   double FaserTruthParticle::pt() const {

      // Do the calculation by hand:
      const double localPx = static_cast< double >( px() );
      const double localPy = static_cast< double >( py() );
      return std::sqrt( localPx * localPx + localPy * localPy );
   }

   double FaserTruthParticle::eta() const {

      // Calculate the pseudo-rapidity using TLorentzVector.
      // Could do something more lightweight later on.
      return p4().Eta();
   }

   double FaserTruthParticle::phi() const {

      // Calculate the azimuth angle using TLorentzVector.
      // Could do something more lightweight later on.
      return p4().Phi();
   }

   AUXSTORE_PRIMITIVE_GETTER_WITH_CAST( FaserTruthParticle, float, double, m )
   AUXSTORE_PRIMITIVE_GETTER_WITH_CAST( FaserTruthParticle, float, double, e )

   double FaserTruthParticle::rapidity() const {

      return p4().Rapidity();
   }

   FaserTruthParticle::FourMom_t FaserTruthParticle::p4() const { 
     return FourMom_t(px(), py(), pz(), e() );
   }

   FaserType::ObjectType FaserTruthParticle::faserType() const {

      return FaserType::FaserTruthParticle;
   }
   
   Type::ObjectType FaserTruthParticle::type() const {

      return Type::Other;
   }

   //
   /////////////////////////////////////////////////////////////////////////////

   /////////////////////////////////////////////////////////////////////////////
   //
   //    Implementation of the truth particle specific 4-momentum functions
   //

   double FaserTruthParticle::abseta() const {

      return std::abs( eta() );
   }

   double FaserTruthParticle::absrapidity() const {

      return std::abs( rapidity() );
   }

   AUXSTORE_PRIMITIVE_GETTER( FaserTruthParticle, float, px )

   void FaserTruthParticle::setPx( float value ) {
      static Accessor< float > acc( "px" );
      acc( *this ) = value;
      return;
   }

   AUXSTORE_PRIMITIVE_GETTER( FaserTruthParticle, float, py )

   void FaserTruthParticle::setPy( float value ) {
      static Accessor< float > acc( "py" );
      acc( *this ) = value;
      return;
   }

   AUXSTORE_PRIMITIVE_GETTER( FaserTruthParticle, float, pz )

   void FaserTruthParticle::setPz( float value ) {
      static Accessor< float > acc( "pz" );
      acc( *this ) = value;
      return;
   }
   
   void FaserTruthParticle::setE( float value ) {
      static Accessor< float > acc( "e" );
      acc( *this ) = value;
      return;
   }
   
   void FaserTruthParticle::setM( float value ) {
      static Accessor< float > acc( "m" );
      // note: this does not invalidate the cache
      acc( *this ) = value;
      return;
   }
   
   //
   /////////////////////////////////////////////////////////////////////////////

   /////////////////////////////////////////////////////////////////////////////
   //
   //         Implementation of the particle species decoder functions
   //

/// Helper macro to implement the functions that rely in functions from MC::PID
#define MC_PID_HELPER( TYPE, FNAME )      \
   TYPE FaserTruthParticle::FNAME() const { \
      return MC::PID::FNAME( pdgId() );   \
   }

   MC_PID_HELPER( double, charge )
   MC_PID_HELPER( int, threeCharge )

   MC_PID_HELPER( bool, isCharged )
   MC_PID_HELPER( bool, isNeutral )

   MC_PID_HELPER( bool, isPhoton )
   MC_PID_HELPER( bool, isLepton )
   MC_PID_HELPER( bool, isChLepton )
   MC_PID_HELPER( bool, isElectron )
   MC_PID_HELPER( bool, isMuon )
   MC_PID_HELPER( bool, isTau )
   MC_PID_HELPER( bool, isNeutrino )

   MC_PID_HELPER( bool, isHadron )
   MC_PID_HELPER( bool, isMeson )
   MC_PID_HELPER( bool, isBaryon )

   MC_PID_HELPER( bool, hasStrange )
   MC_PID_HELPER( bool, hasCharm )
   MC_PID_HELPER( bool, hasBottom )

   MC_PID_HELPER( bool, isLightMeson )
   MC_PID_HELPER( bool, isLightBaryon )
   MC_PID_HELPER( bool, isLightHadron )

   MC_PID_HELPER( bool, isHeavyMeson )
   MC_PID_HELPER( bool, isHeavyBaryon )
   MC_PID_HELPER( bool, isHeavyHadron )

   MC_PID_HELPER( bool, isBottomMeson )
   MC_PID_HELPER( bool, isBottomBaryon )
   MC_PID_HELPER( bool, isBottomHadron )

   MC_PID_HELPER( bool, isCharmMeson )
   MC_PID_HELPER( bool, isCharmBaryon )
   MC_PID_HELPER( bool, isCharmHadron )

   MC_PID_HELPER( bool, isStrangeMeson )
   MC_PID_HELPER( bool, isStrangeBaryon )
   MC_PID_HELPER( bool, isStrangeHadron )

   MC_PID_HELPER( bool, isQuark )
   MC_PID_HELPER( bool, isParton )
   MC_PID_HELPER( bool, isTop )
   MC_PID_HELPER( bool, isW )
   MC_PID_HELPER( bool, isZ )
   MC_PID_HELPER( bool, isHiggs )
   MC_PID_HELPER( bool, isResonance )
   MC_PID_HELPER( bool, isGenSpecific )

// Forget about this macro:
#undef MC_PID_HELPER

   //
   /////////////////////////////////////////////////////////////////////////////

   /////////////////////////////////////////////////////////////////////////////
   //
   //          Implementation of the optional polarization accessors
   //

   bool FaserTruthParticle::polarizationParameter( float& value,
                                                 PolParam param ) const {

      // Get the accessor object:
      Accessor< float >* acc = polarizationAccessor( param );
      if( ! acc ) {
         // The user asked for a non-existent parameter type. o.O
         std::cerr << "xAOD::FaserTruthParticle::polarizationParameter ERROR "
                   << "Request for an unknown (" << param << ") polarization "
                   << "parameter type" << std::endl;
         return false;
      }
      // Check if the variable is available:
      if( ! acc->isAvailable( *this ) ) {
         // No, it is not.
         return false;
      }

      // Read the value:
      value = ( *acc )( *this );
      return true;
   }

   bool FaserTruthParticle::setPolarizationParameter( float value,
                                                    PolParam param ) {

      // Get the accessor object:
      Accessor< float >* acc = polarizationAccessor( param );
      if( ! acc ) {
         // The user asked for a non-existent parameter type. o.O
         std::cerr << "xAOD::FaserTruthParticle::setPolarizationParameter ERROR "
                   << "Request for an unknown (" << param << ") polarization "
                   << "parameter type" << std::endl;
         return false;
      }

      // Set the value:
      ( *acc )( *this ) = value;
      return true;
   }

   float FaserTruthParticle::polarizationPatameter( PolParam param ) const {

      // Get the accessor object:
      Accessor< float >* acc = polarizationAccessor( param );
      if( ! acc ) {
         // Throw an exception:
         throw std::runtime_error( "Unrecognized polarization parameter "
                                   "requested" );
      }

      // Read the value:
      return ( *acc )( *this );
   }

   FaserTruthParticle::Polarization FaserTruthParticle::polarization() const {

      // Construct the object:
      Polarization rtn;
      polarizationParameter( rtn.phi, polarizationPhi );
      polarizationParameter( rtn.theta, polarizationTheta );

      return rtn;
   }

   //
   /////////////////////////////////////////////////////////////////////////////

   void FaserTruthParticle::toPersistent() {

      if( prodVtxLinkAcc.isAvailableWritable( *this ) ) {
         prodVtxLinkAcc( *this ).toPersistent();
      }
      if( decayVtxLinkAcc.isAvailableWritable( *this ) ) {
         decayVtxLinkAcc( *this ).toPersistent();
      }
      return;
   }

} // namespace xAOD
