# Declare the package name:
atlas_subdir( xAODFaserTracking )

# External dependencies:
find_package( Eigen )
find_package( ROOT COMPONENTS Core GenVector )

# Component(s) in the package:
atlas_add_library( xAODFaserTracking
   xAODFaserTracking/*.h Root/*.cxx
   PUBLIC_HEADERS xAODFaserTracking
   INCLUDE_DIRS ${EIGEN_INCLUDE_DIRS}
   LINK_LIBRARIES ${EIGEN_LIBRARIES} ${ROOT_LIBRARIES} AthContainers AthLinks xAODBase xAODFaserBase xAODCore ${extra_libs} )

atlas_add_dictionary( xAODFaserTrackingDict
   xAODFaserTracking/xAODFaserTrackingDict.h
   xAODFaserTracking/selection.xml
   LINK_LIBRARIES xAODFaserTracking
   EXTRA_FILES Root/dict/*.cxx )

# Test(s) in the package:
atlas_add_test( xAODFaserTracking_StripCluster_test
   SOURCES test/xAODFaserTracking_StripCluster_test.cxx
   LINK_LIBRARIES xAODFaserTracking )
   
atlas_add_test( xAODFaserTracking_StripRawData_test
   SOURCES test/xAODFaserTracking_StripRawData_test.cxx
   LINK_LIBRARIES xAODFaserTracking )
    
atlas_add_test( xAODFaserTracking_Track_test
   SOURCES test/xAODFaserTracking_Track_test.cxx
   LINK_LIBRARIES xAODFaserTracking )