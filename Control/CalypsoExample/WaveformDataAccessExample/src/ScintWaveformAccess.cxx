/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

/*
 * ScintWaveformAccess.cxx
 * 
 * Simple algorithm to access waveform data from storegate.
 * Try to write a proper thread-safe algorithm.
 *
 */

#include "ScintWaveformAccess.h"
//#include "ScintRawEvent/ScintWaveform.h"

ScintWaveformAccess::ScintWaveformAccess(const std::string& name, ISvcLocator* pSvcLocator) : AthReentrantAlgorithm(name, pSvcLocator)
{
}

ScintWaveformAccess::~ScintWaveformAccess()
{
}

StatusCode
ScintWaveformAccess::initialize()
{
  ATH_MSG_DEBUG("ScintWaveformAccess::initialize() called");

  // Must initialize SG handles
  ATH_CHECK( m_CaloWaveformContainer.initialize() );
  ATH_CHECK( m_VetoWaveformContainer.initialize() );
  ATH_CHECK( m_TriggerWaveformContainer.initialize() );
  ATH_CHECK( m_PreshowerWaveformContainer.initialize() );
  ATH_CHECK( m_TestWaveformContainer.initialize() );
  ATH_CHECK( m_ClockWaveformContainer.initialize() );

  return StatusCode::SUCCESS;
}

StatusCode
ScintWaveformAccess::finalize()
{
  ATH_MSG_DEBUG("ScintWaveformAccess::finalize() called");

  return StatusCode::SUCCESS;
}

StatusCode
ScintWaveformAccess::execute(const EventContext& ctx) const
{
  ATH_MSG_DEBUG("ScintWaveformAccess::execute() called");

  // Try reading all of the different containers
  SG::ReadHandle<ScintWaveformContainer> caloHandle(m_CaloWaveformContainer, ctx);
  ATH_MSG_INFO("Found ReadHandle for CaloWaveforms");
  ATH_MSG_INFO(*caloHandle);

  SG::ReadHandle<ScintWaveformContainer> vetoHandle(m_VetoWaveformContainer, ctx);
  ATH_MSG_INFO("Found ReadHandle for VetoWaveforms");
  ATH_MSG_INFO(*vetoHandle);

  SG::ReadHandle<ScintWaveformContainer> triggerHandle(m_TriggerWaveformContainer, ctx);
  ATH_MSG_INFO("Found ReadHandle for TriggerWaveforms");
  ATH_MSG_INFO(*triggerHandle);

  SG::ReadHandle<ScintWaveformContainer> preshowerHandle(m_PreshowerWaveformContainer, ctx);
  ATH_MSG_INFO("Found ReadHandle for PreshowerWaveforms");
  ATH_MSG_INFO(*preshowerHandle);

  SG::ReadHandle<ScintWaveformContainer> testHandle(m_TestWaveformContainer, ctx);
  ATH_MSG_INFO("Found ReadHandle for TestWaveforms");
  ATH_MSG_INFO(*testHandle);

  SG::ReadHandle<ScintWaveformContainer> clockHandle(m_ClockWaveformContainer, ctx);
  ATH_MSG_INFO("Found ReadHandle for ClockWaveforms");
  ATH_MSG_INFO(*clockHandle);

  return StatusCode::SUCCESS;
}
