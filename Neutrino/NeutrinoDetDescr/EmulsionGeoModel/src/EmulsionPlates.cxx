/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "EmulsionPlates.h"

#include "EmulsionGeometryManager.h"
#include "EmulsionMaterialManager.h"
#include "EmulsionGeneralParameters.h"
#include "EmulsionPlatesParameters.h"

#include "GeoModelKernel/GeoBox.h"
#include "GeoModelKernel/GeoLogVol.h"
#include "GeoModelKernel/GeoFullPhysVol.h"
#include "GeoModelKernel/GeoMaterial.h"
#include "GeoModelKernel/GeoPhysVol.h"
#include "GeoModelKernel/GeoVPhysVol.h"
#include "GeoModelKernel/GeoNameTag.h"
#include "GeoModelKernel/GeoIdentifierTag.h"
#include "GeoModelKernel/GeoTransform.h"
#include "GeoModelKernel/GeoAlignableTransform.h"

#include "NeutrinoReadoutGeometry/EmulsionDetectorManager.h"
#include "NeutrinoReadoutGeometry/NeutrinoDetectorDesign.h"
#include "NeutrinoReadoutGeometry/NeutrinoDetectorElement.h"
#include "NeutrinoReadoutGeometry/NeutrinoDD_Defs.h"
#include "NeutrinoReadoutGeometry/NeutrinoCommonItems.h"

#include "GaudiKernel/SystemOfUnits.h"

using namespace NeutrinoDD;

EmulsionPlates::EmulsionPlates(const std::string & name,
                     NeutrinoDD::EmulsionDetectorManager* detectorManager,
                     const EmulsionGeometryManager* geometryManager,
                     EmulsionMaterialManager* materials)
  : EmulsionUniqueComponentFactory(name, detectorManager, geometryManager, materials)
{
  getParameters();
  m_logVolume = preBuild();
}


void
EmulsionPlates::getParameters()
{
  const EmulsionGeneralParameters * generalParameters = m_geometryManager->generalParameters();
  const EmulsionPlatesParameters *   platesParameters = m_geometryManager->platesParameters();

  m_width = platesParameters->platesWidth();
  m_height = platesParameters->platesHeight();
  m_thickness = platesParameters->platesThickness();
  m_material = m_materials->getMaterial(platesParameters->platesMaterial());

  m_nBasesPerModule = generalParameters->nBasesPerModule();
  m_nModules       = generalParameters->nModules();
  m_firstBaseZ     = generalParameters->firstBaseZ();
  m_lastBaseZ      = generalParameters->lastBaseZ();

  m_detectorManager->numerology().setNumBasesPerModule(m_nBasesPerModule);
}

const GeoLogVol * 
EmulsionPlates::preBuild()
{
  // create child element
  m_base = new EmulsionBase("Base", m_detectorManager, m_geometryManager, m_materials);


  // Build a box to hold everything
  const GeoBox* platesShape = new GeoBox(0.5*m_width, 0.5*m_height, 0.5*m_thickness);

  // GeoLogVol * platesLog = new GeoLogVol(getName(), platesShape, m_materials->gasMaterial());
  GeoLogVol * platesLog = new GeoLogVol(getName(), platesShape, m_material);

  // m_baseboardPos = new GeoTrf::Translate3D(0.0, 0.0, 0.0);
  // m_frontPos     = new GeoTrf::Translate3D(0.0, 0.0, -(m_baseThickness + m_filmThickness)/2);
  // m_backPos     = new GeoTrf::Translate3D(0.0, 0.0, (m_baseThickness + m_filmThickness)/2);

  return platesLog;
}

GeoVPhysVol * 
EmulsionPlates::build(EmulsionIdentifier id)
{
  GeoFullPhysVol * plates = new GeoFullPhysVol(m_logVolume); 
  
  int nBases = 0;
  int nBasesTotal = m_nModules * m_nBasesPerModule;
  // Loop over modules
  for (int module = 0; module < m_nModules; module++)
  {
    id.setModule(module);
    // plates->add(new GeoNameTag("Module#"+intToString(module)));
    // plates->add(new GeoIdentifierTag(module));
    // Loop over bases in a module
    for (int base = 0; base < m_nBasesPerModule; base++)
    {
      id.setBase(base);
      GeoAlignableTransform* theTransform = new GeoAlignableTransform( GeoTrf::Translate3D {0.0, 0.0, m_firstBaseZ + ((m_lastBaseZ - m_firstBaseZ)/(nBasesTotal-1))*nBases++} );
      plates->add(theTransform);
      plates->add(new GeoNameTag("Base#"+intToString(module*100 + base)));
      plates->add(new GeoIdentifierTag(module*100 + base));
      GeoVPhysVol* physBase = m_base->build(id);
      plates->add(physBase);
      m_detectorManager->addAlignableTransform(1, id.getFilmId(), theTransform, physBase);
    }
    m_detectorManager->numerology().useModule(module);
  }

  return plates;
}
