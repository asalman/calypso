###############################################################################
# Package: NeutrinoSimEventTPCnv
################################################################################

# Declare the package name:
atlas_subdir( NeutrinoSimEventTPCnv )

# External dependencies:
find_package( CLHEP )
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread )

# Component(s) in the package:
atlas_add_library( NeutrinoSimEventTPCnv
                   src/NeutrinoHits/*.cxx
                   PUBLIC_HEADERS NeutrinoSimEventTPCnv
                   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
                   PRIVATE_DEFINITIONS ${CLHEP_DEFINITIONS}
                   LINK_LIBRARIES GaudiKernel GeneratorObjectsTPCnv NeutrinoSimEvent AthenaPoolCnvSvcLib StoreGateLib SGtests
                   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} ${CLHEP_LIBRARIES} TestTools Identifier )

atlas_add_dictionary( NeutrinoSimEventTPCnvDict
                      NeutrinoSimEventTPCnv/NeutrinoSimEventTPCnvDict.h
                      NeutrinoSimEventTPCnv/selection.xml
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} ${CLHEP_LIBRARIES} AthenaPoolCnvSvcLib GaudiKernel GeneratorObjectsTPCnv NeutrinoSimEvent TestTools StoreGateLib SGtests Identifier NeutrinoSimEventTPCnv )

