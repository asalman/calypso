#include "FaserActsKalmanFilter/CombinatorialKalmanFilterAlg.h"
#include "FaserActsGeometry/FASERMagneticFieldWrapper.h"

#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Navigator.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/TrackFitting/GainMatrixSmoother.hpp"
#include "Acts/TrackFitting/GainMatrixUpdater.hpp"
#include "Acts/MagneticField/ConstantBField.hpp"
#include "Acts/MagneticField/InterpolatedBFieldMap.hpp"
#include "Acts/MagneticField/SharedBField.hpp"

#include <boost/variant/variant.hpp>
#include <boost/variant/apply_visitor.hpp>
#include <boost/variant/static_visitor.hpp>


using Updater = Acts::GainMatrixUpdater;
using Smoother = Acts::GainMatrixSmoother;
using Stepper = Acts::EigenStepper<FASERMagneticFieldWrapper>;
using Propagator = Acts::Propagator<Stepper, Acts::Navigator>;

namespace ActsExtrapolationDetail {
  using VariantPropagatorBase = boost::variant<
      Acts::Propagator<Acts::EigenStepper<FASERMagneticFieldWrapper>, Acts::Navigator>,
      Acts::Propagator<Acts::EigenStepper<Acts::ConstantBField>, Acts::Navigator>
  >;

  class VariantPropagator : public VariantPropagatorBase
  {
  public:
    using VariantPropagatorBase::VariantPropagatorBase;
  };

}

using ActsExtrapolationDetail::VariantPropagator;


template <typename TrackFinder>
struct TrackFinderFunctionImpl {
  TrackFinder trackFinder;

  TrackFinderFunctionImpl(TrackFinder&& f) : trackFinder(std::move(f)) {}

  CombinatorialKalmanFilterAlg::TrackFinderResult operator()(
      const IndexSourceLinkContainer& sourceLinks,
      const std::vector<Acts::CurvilinearTrackParameters>& initialParameters,
      const CombinatorialKalmanFilterAlg::TrackFinderOptions& options) const
  {
    return trackFinder.findTracks(sourceLinks, initialParameters, options);
  }
};


CombinatorialKalmanFilterAlg::TrackFinderFunction 
CombinatorialKalmanFilterAlg::makeTrackFinderFunction(
    std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry)
{
  const std::string fieldMode = "FASER";
  const std::vector<double> constantFieldVector = {0., 0., 0.55};

  Acts::Navigator navigator(trackingGeometry);
  navigator.resolvePassive   = false;
  navigator.resolveMaterial  = true;
  navigator.resolveSensitive = true;

  ActsExtrapolationDetail::VariantPropagator* varProp {nullptr};

  if (fieldMode == "FASER") {
    using BField_t = FASERMagneticFieldWrapper;
    BField_t bField;
    auto stepper = Acts::EigenStepper<BField_t>(std::move(bField));
    auto propagator = Acts::Propagator<decltype(stepper), Acts::Navigator>(std::move(stepper), std::move(navigator));
    varProp = new VariantPropagator(propagator);
  }
  else if (fieldMode == "Constant") {
    std::vector<double> constantFieldVector = constantFieldVector;
    double Bx = constantFieldVector.at(0);
    double By = constantFieldVector.at(1);
    double Bz = constantFieldVector.at(2);
    using BField_t = Acts::ConstantBField;
    BField_t bField(Bx, By, Bz);
    auto stepper = Acts::EigenStepper<BField_t>(std::move(bField));
    auto propagator = Acts::Propagator<decltype(stepper), Acts::Navigator>(std::move(stepper),
     std::move(navigator));
    varProp = new VariantPropagator(propagator);
  }

  return boost::apply_visitor([&](const auto& propagator) -> TrackFinderFunction {
    using Updater  = Acts::GainMatrixUpdater;
    using Smoother = Acts::GainMatrixSmoother;
    using CKF = Acts::CombinatorialKalmanFilter<typename std::decay_t<decltype(propagator)>, Updater, Smoother>;

    CKF trackFinder(std::move(propagator));

    return TrackFinderFunctionImpl<CKF>(std::move(trackFinder));
  }, *varProp);
}