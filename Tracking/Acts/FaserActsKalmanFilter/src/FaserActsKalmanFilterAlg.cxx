/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "FaserActsKalmanFilter/FaserActsKalmanFilterAlg.h"

// ATHENA
#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "TrackerReadoutGeometry/SCT_DetectorManager.h"
#include "TrackerReadoutGeometry/SiDetectorDesign.h"
#include "TrackerReadoutGeometry/SiLocalPosition.h"
#include "TrackerReadoutGeometry/SiDetectorElement.h"
#include "TrackerSpacePoint/FaserSCT_SpacePoint.h"
#include "TrackerSpacePoint/SpacePointForSeedCollection.h"
#include "TrackerIdentifier/FaserSCT_ID.h"
#include "FaserDetDescr/FaserDetectorID.h"

// ACTS
#include "Acts/TrackFitting/GainMatrixSmoother.hpp"
#include "Acts/TrackFitting/GainMatrixUpdater.hpp"
#include "Acts/Geometry/GeometryIdentifier.hpp"
#include "Acts/MagneticField/ConstantBField.hpp"
#include "Acts/MagneticField/InterpolatedBFieldMap.hpp"
#include "Acts/MagneticField/SharedBField.hpp"
#include "Acts/MagneticField/MagneticFieldContext.hpp"
#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Navigator.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/Surfaces/Surface.hpp"
#include "Acts/Surfaces/PlaneSurface.hpp"
#include "Acts/Surfaces/PerigeeSurface.hpp"
#include "Acts/Surfaces/RectangleBounds.hpp"
#include "Acts/Definitions/Units.hpp"
#include "Acts/Utilities/Logger.hpp"
#include "Acts/Utilities/Helpers.hpp"
#include "Acts/Utilities/detail/periodic.hpp"
#include "Acts/Definitions/Common.hpp"
#include "Acts/Definitions/Algebra.hpp"
#include "Acts/Definitions/TrackParametrization.hpp"
#include "Acts/Utilities/CalibrationContext.hpp"
#include "Acts/EventData/TrackParameters.hpp"
#include "Acts/EventData/MultiTrajectoryHelpers.hpp"
#include "Acts/EventData/Measurement.hpp"
#include "Acts/Geometry/GeometryIdentifier.hpp"


// PACKAGE
#include "FaserActsGeometryInterfaces/IFaserActsTrackingGeometryTool.h"
#include "ActsInterop/Logger.h"
#include "FaserActsGeometry/FaserActsGeometryContext.h"
#include "FaserActsGeometry/IFaserActsPropStepRootWriterSvc.h"
#include "FaserActsGeometry/FaserActsDetectorElement.h"

//ROOT
#include <TTree.h>

// BOOST
#include <boost/variant/variant.hpp>
#include <boost/variant/apply_visitor.hpp>
// #include <boost/variant/static_visitor.hpp>

// STL
#include <string>
#include <fstream>
#include <cmath>
#include <random>

using TrajectoryContainer = std::vector<FaserActsRecMultiTrajectory>;

using namespace Acts::UnitLiterals;
using Acts::VectorHelpers::eta;
using Acts::VectorHelpers::perp;
using Acts::VectorHelpers::phi;
using Acts::VectorHelpers::theta;
using ThisMeasurement = Acts::Measurement<IndexSourceLink, Acts::BoundIndices, 2>;
using Updater = Acts::GainMatrixUpdater;
using Smoother = Acts::GainMatrixSmoother;
using Stepper = Acts::EigenStepper<FASERMagneticFieldWrapper>;
using Propagator = Acts::Propagator<Stepper, Acts::DirectNavigator>;
using Fitter = Acts::KalmanFitter<Propagator, Updater, Smoother>;

namespace ActsExtrapolationDetail {
  using VariantPropagatorBase = boost::variant<
      Acts::Propagator<Acts::EigenStepper<FASERMagneticFieldWrapper>, Acts::DirectNavigator>,
      Acts::Propagator<Acts::EigenStepper<Acts::ConstantBField>, Acts::DirectNavigator>
  >;

  class VariantPropagator : public VariantPropagatorBase
  {
  public:
    using VariantPropagatorBase::VariantPropagatorBase;
  };

}

using ActsExtrapolationDetail::VariantPropagator;

FaserActsKalmanFilterAlg::FaserActsKalmanFilterAlg(const std::string& name, ISvcLocator* pSvcLocator)
    : AthAlgorithm(name, pSvcLocator) , m_thistSvc("THistSvc", name)
{
}

// FixMe the Identifier to GeometryIdentfier mapping should not be hardcoded.
int FaserActsKalmanFilterAlg::getGeometryIdentifierVolume(int station)
{
  switch(station)
  {
    case 1:
      return 2;
    case 2:
      return 3;
    case 3:
      return 4;
    default:
      ATH_MSG_ERROR("Received unexpected station. Check detector geometry");
      return 0;
  }
}

int FaserActsKalmanFilterAlg::getGeometryIdentifierLayer(int layer)
{
  switch(layer)
  {
    case 0:
      return 2;
    case 1:
      return 4;
    case 2:
      return 6;
    default:
      ATH_MSG_ERROR("Received unexpected layer. Check detector geometry");
      return 0;
  }
}

int FaserActsKalmanFilterAlg::getGeometryIdentifierSensitive(int row, int column)
{
  if (row == 0 && column == -1)
    return 2;
  else if (row == 0 && column == 1)
    return 4;
  else if (row == 1 && column == -1)
    return 5;
  else if (row == 1 && column == 1)
    return 7;
  else if (row == 2 && column == -1)
    return 10;
  else if (row == 2 && column == 1)
    return 12;
  else if (row == 3 && column == -1)
    return 13;
  else if (row ==3 && column == 1)
    return 15;
  else
  {
    ATH_MSG_ERROR("Received unexpected row or column. Check detector geometry");
    return 0;
  }
}

Acts::GeometryIdentifier FaserActsKalmanFilterAlg::getGeometryIdentifier(const Identifier id)
{
  Acts::GeometryIdentifier geoId = Acts::GeometryIdentifier();
  geoId.setVolume(getGeometryIdentifierVolume(m_idHelper->station(id)));
  geoId.setLayer(getGeometryIdentifierLayer(m_idHelper->layer(id)));
  geoId.setSensitive(getGeometryIdentifierSensitive(m_idHelper->phi_module(id), m_idHelper->eta_module(id)));
  return geoId;
}

StatusCode FaserActsKalmanFilterAlg::initialize() {

  ATH_MSG_DEBUG(name() << "::" << __FUNCTION__);

  ATH_MSG_INFO("Initializing ACTS kalman filter");

      ATH_CHECK( m_fieldCondObjInputKey.initialize() );

      ATH_CHECK( m_extrapolationTool.retrieve() );

  if ( m_seed_spcollectionKey.key().empty()){
    ATH_MSG_FATAL( "SCTs selected and no name set for SCT clusters");
    return StatusCode::FAILURE;
  }

      ATH_CHECK( m_seed_spcollectionKey.initialize() );

      ATH_CHECK( m_mcEventKey.initialize() );

      ATH_CHECK( m_sctMap.initialize());

      ATH_CHECK(detStore()->retrieve(m_idHelper,"FaserSCT_ID"));

      ATH_CHECK(detStore()->retrieve(m_detManager, "SCT"));

      ATH_CHECK(m_thistSvc.retrieve());

  m_trackTree = new TTree("tracks", "");

      ATH_CHECK(m_thistSvc->regTree("/KalmanTracks/tracks", m_trackTree));

  if (m_trackTree) {
    initializeTree();
  }
  else {
    ATH_MSG_ERROR("No tree found!");
  }

  ATH_MSG_INFO("ACTS kalman filter successfully initialized");
  return StatusCode::SUCCESS;
}

//StatusCode FaserActsKalmanFilterAlg::execute(const EventContext& ctx) const
StatusCode FaserActsKalmanFilterAlg::execute()
{

  ATH_MSG_VERBOSE(name() << "::" << __FUNCTION__);

  m_eventNr++;

  //SG::ReadHandle<SpacePointForSeedCollection> seed_spcollection( m_seed_spcollectionKey, ctx );
  SG::ReadHandle<SpacePointForSeedCollection> seed_spcollection( m_seed_spcollectionKey );
  if (!seed_spcollection.isValid()){
    msg(MSG:: FATAL) << "Could not find the data object "<< seed_spcollection.name() << " !" << endmsg;
    return StatusCode::RECOVERABLE;
  }

  const FaserActsGeometryContext& gctx
      = m_extrapolationTool->trackingGeometryTool()->getNominalGeometryContext();
  auto geoctx = gctx.context();
  Acts::MagneticFieldContext magctx = getMagneticFieldContext();
  Acts::CalibrationContext calctx;

  std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry
      = m_extrapolationTool->trackingGeometryTool()->trackingGeometry();

  std::shared_ptr<const Acts::Surface> pSurface;

  // Get SiDetectorElements
  const TrackerDD::SiDetectorElementCollection* elCollection{m_detManager->getDetectorElementCollection()};
  if (elCollection == nullptr) {
    ATH_MSG_FATAL("Null pointer is returned by getDetectorElementCollection()");
    return StatusCode::FAILURE;
  }


  static const TrackerDD::SCT_DetectorManager     *s_sct;
  if(detStore()->retrieve(s_sct,"SCT").isFailure()) s_sct = 0;
  int N_1_0=0, N_1_1=0, N_1_2=0, N_2_0=0, N_2_1=0, N_2_2=0;
  Acts::Vector3 pos1_0(0., 0., 0.);
  Acts::Vector3 pos1_1(0., 0., 0.);
  Acts::Vector3 pos1_2(0., 0., 0.);
  Acts::Vector3 pos2_0(0., 0., 0.);
  Acts::Vector3 pos2_1(0., 0., 0.);
  Acts::Vector3 pos2_2(0., 0., 0.);
  HepMC::FourVector truthmom;
  HepMC::FourVector pv;

  // create source links and measurements
  std::vector<IndexSourceLink> sourceLinks;
  std::vector<const Acts::Surface*> surfSequence;
  MeasurementContainer measurements;

  SpacePointForSeedCollection::const_iterator it = seed_spcollection->begin();
  SpacePointForSeedCollection::const_iterator itend = seed_spcollection->end();
  for (; it != itend; ++it){
    const Trk::SpacePoint *sp = (&(**it))->SpacePoint();

    const Identifier id = sp->clusterList().first->identify();
    const TrackerDD::SiDetectorElement* siSpElement = m_detManager->getDetectorElement(id);
    auto spElement = static_cast<const FaserActsDetectorElement>(siSpElement);
    // Acts::GeometryIdentifier geoId = getGeometryIdentifier(id);
    Acts::GeometryIdentifier geoId;

    const Acts::TrackingVolume* tVolume = trackingGeometry->highestTrackingVolume();
    if (tVolume->confinedVolumes()) {
      for (auto volume : tVolume->confinedVolumes()->arrayObjects()) {
        if (volume->confinedLayers()) {
          for (const auto& layer : volume->confinedLayers()->arrayObjects()) {
            if (layer->layerType() == Acts::navigation) continue;
            for (auto surface : layer->surfaceArray()->surfaces()) {
              if (surface) {
                const Acts::DetectorElementBase *detElement = surface->associatedDetectorElement();
                const auto *faserDetElement = dynamic_cast<const FaserActsDetectorElement*>(detElement);
                auto* tmp = const_cast<FaserActsDetectorElement*>(faserDetElement);
                if (*tmp == spElement) {
                  geoId = surface->geometryId();
                }
              }
            }
          }
        }
      }
    }

    const Acts::Surface* surfacePtr = trackingGeometry->findSurface(geoId);
    surfSequence.push_back(surfacePtr);
    if (not surfacePtr) {
      ATH_MSG_ERROR("Could not find surface " << geoId);
      return StatusCode::FAILURE;
    }

    Index spIdx = measurements.size();
    IndexSourceLink sourceLink(geoId, spIdx);


    auto par = sp->localParameters();
    auto cov = sp->localCovariance();
    std::array<Acts::BoundIndices, 2> indices = {Acts::eBoundLoc0, Acts::eBoundLoc1};
    ThisMeasurement meas(sourceLink, indices, par, cov);
    sourceLinks.push_back(std::move(sourceLink));
    measurements.emplace_back(std::move(meas));


    // Get the truth position and momentum
    SG::ReadHandle<McEventCollection> h_mcEvents(m_mcEventKey);
    SG::ReadHandle<TrackerSimDataCollection> h_collectionMap(m_sctMap);
    const auto& simdata = h_collectionMap->find(id)->second;
    const auto& deposits = simdata.getdeposits();
    for( const auto& depositPair : deposits)
    {
      if (depositPair.first->pdg_id() == -13) {
        pv = depositPair.first->production_vertex()->position();
        truthmom = depositPair.first->momentum();
      }
    }

    // Get the measurements
    Amg::Vector3D gloPos=sp->globalPosition();
    int station = m_idHelper->station(id);
    int plane = m_idHelper->layer(id);
    if (station==1 && plane==0) {
      N_1_0++;
      pos1_0 = Acts::Vector3(gloPos.x(), gloPos.y(), gloPos.z());

      // Construct a plane surface as the target surface
      const TrackerDD::SiDetectorDesign &design = siSpElement->design();
      double hlX = design.width()/2. * 1_mm;
      double hlY = design.length()/2. * 1_mm;
      auto rectangleBounds = std::make_shared<const Acts::RectangleBounds>(hlX, hlY);
      Amg::Transform3D g2l = siSpElement->getMaterialGeom()->getDefAbsoluteTransform()
                             * Amg::CLHEPTransformToEigen(siSpElement->recoToHitTransform());
      pSurface = Acts::Surface::makeShared<Acts::PlaneSurface>( g2l, rectangleBounds );
    }
    if (station==1 && plane==1) {
      N_1_1++;
      pos1_1 = Acts::Vector3(gloPos.x(), gloPos.y(), gloPos.z());
    }
    if (station==1 && plane==2) {
      N_1_2++;
      pos1_2 = Acts::Vector3(gloPos.x(), gloPos.y(), gloPos.z());
    }
    if (station==2 && plane==0) {
      N_2_0++;
      pos2_0 = Acts::Vector3(gloPos.x(), gloPos.y(), gloPos.z());
    }
    if (station==2 && plane==1) {
      N_2_1++;
      pos2_1 = Acts::Vector3(gloPos.x(), gloPos.y(), gloPos.z());
    }
    if (station==2 && plane==2) {
      N_2_2++;
      pos2_2 = Acts::Vector3(gloPos.x(), gloPos.y(), gloPos.z());
    }
  }

  for (auto sl : sourceLinks)
  {
    std::cout << "??volume=" << sl.geometryId().volume() << std::endl;
    std::cout << "??layer=" << sl.geometryId().layer() << std::endl;
    std::cout << "??sensitive=" << sl.geometryId().sensitive() << std::endl;
  }

  // Calculate the initial track parameters
  if ( (N_1_0==1) && (N_1_1==1) && (N_1_2==1) && (N_2_0==1) && (N_2_1==1) && (N_2_2==1)) {
    std::cout<<"!!!!!!!!!!!  pos1_0 = ("<<pos1_0.x()<<", "<<pos1_0.y()<<", "<<pos1_0.z()<<") "<<std::endl;
    std::cout<<"!!!!!!!!!!!  pos1_1 = ("<<pos1_1.x()<<", "<<pos1_1.y()<<", "<<pos1_1.z()<<") "<<std::endl;
    std::cout<<"!!!!!!!!!!!  pos1_2 = ("<<pos1_2.x()<<", "<<pos1_2.y()<<", "<<pos1_2.z()<<") "<<std::endl;
    std::cout<<"!!!!!!!!!!!  pos2_0 = ("<<pos2_0.x()<<", "<<pos2_0.y()<<", "<<pos2_0.z()<<") "<<std::endl;
    std::cout<<"!!!!!!!!!!!  pos2_1 = ("<<pos2_1.x()<<", "<<pos2_1.y()<<", "<<pos2_1.z()<<") "<<std::endl;
    std::cout<<"!!!!!!!!!!!  pos2_2 = ("<<pos2_2.x()<<", "<<pos2_2.y()<<", "<<pos2_2.z()<<") "<<std::endl;
    //@FIXME: change the hard codes in future
    double charge = 1;
    double B = 0.55;
    //const Acts::Vector3 pos = pos1_0;
    const Acts::Vector3 pos(pos1_0.x(), pos1_0.y(), pos1_0.z()-1);
    Acts::Vector3 d1 = pos1_2 - pos1_0;
    Acts::Vector3 d2 = pos2_2 - pos2_0;
    // the direction of momentum in the first station
    Acts::Vector3 direct1 = d1.normalized();
    // the direction of momentum in the second station
    Acts::Vector3 direct2 = d2.normalized();
    // the vector pointing from the center of circle to the particle at layer 2 in Y-Z plane
    double R1_z = charge * direct1.y() / std::sqrt(direct1.y()*direct1.y() + direct1.z()*direct1.z());
    // double R1_y = -charge * direct1.z() / std::sqrt(direct1.y()*direct1.y() + direct1.z()*direct1.z());
    // the vector pointing from the center of circle to the particle at layer 3 in Y-Z plane
    double R2_z = charge * direct2.y() / std::sqrt(direct2.y()*direct2.y() + direct2.z()*direct2.z());
    // double R2_y = -charge * direct2.z() / std::sqrt(direct2.y()*direct2.y() + direct2.z()*direct2.z());
    // the norm of radius
    double R = (pos2_0.z() - pos1_2.z()) / (R2_z - R1_z);
    // the norm of momentum in Y-Z plane
    double p_yz = 0.3*B*R / 1000.0;  // R(mm), p(GeV), B(T)
    double p_z = p_yz * direct1.z() / std::sqrt(direct1.y()*direct1.y() + direct1.z()*direct1.z());
    double p_y = p_yz * direct1.y() / std::sqrt(direct1.y()*direct1.y() + direct1.z()*direct1.z());
    double p_x = direct1.x() * p_z / direct1.z();
    // total momentum at the layer 0
    const Acts::Vector3 mom(p_x, p_y, p_z);
    double p = mom.norm();
    std::cout<<"!!!!!!!!!!!  InitTrack momentum on layer 0: ( "<<mom.x()*1000<<",  "<<mom.y()*1000<<",  "<<mom.z()*1000<<",  "<<p*1000<<")  "<<std::endl;
    // build the track covariance matrix using the smearing sigmas
    double sigmaU = 200_um;
    double sigmaV = 200_um;
    double sigmaPhi = 1_degree;
    double sigmaTheta = 1_degree;
    double sigmaQOverP = 0.01*p / (p*p);
    double sigmaT0 = 1_ns;
    Acts::BoundSymMatrix cov = Acts::BoundSymMatrix::Zero();
    cov(Acts::eBoundLoc0, Acts::eBoundLoc0) = sigmaU * sigmaU;
    cov(Acts::eBoundLoc1, Acts::eBoundLoc1) = sigmaV * sigmaV;
    cov(Acts::eBoundPhi, Acts::eBoundPhi) = sigmaPhi * sigmaPhi;
    cov(Acts::eBoundTheta, Acts::eBoundTheta) = sigmaTheta * sigmaTheta;
    cov(Acts::eBoundQOverP, Acts::eBoundQOverP) = sigmaQOverP * sigmaQOverP;
    cov(Acts::eBoundTime, Acts::eBoundTime) = sigmaT0 * sigmaT0;
    double time =0;
    //Acts::CurvilinearTrackParameters InitTrackParam(std::make_optional(std::move(cov)), pos, mom, charge, time); // calculated initial parameters

    // Smearing truth parameters as initial parameters
    Acts::Vector3 pPos(pv.x(), pv.y(), pv.z());
    Acts::Vector3 pMom(truthmom.x()/1000., truthmom.y()/1000., truthmom.z()/1000.);
    std::random_device rd;
    std::default_random_engine rng {rd()};
    std::normal_distribution<> norm; // mu: 0 sigma: 1
    Acts::Vector3 deltaPos(sigmaU*norm(rng), sigmaU*norm(rng), sigmaU*norm(rng));
    auto theta = Acts::VectorHelpers::theta(pMom.normalized());
    auto phi = Acts::VectorHelpers::phi(pMom.normalized());
    auto angles = Acts::detail::normalizePhiTheta(phi + sigmaPhi*norm(rng), theta + sigmaTheta*norm(rng));
    Acts::Vector3 dir(std::sin(angles.second) * std::cos(angles.first),
                      std::sin(angles.second) * std::sin(angles.first),
                      std::cos(angles.second));
    const Acts::Vector3 deltaMom = ( pMom.norm()*(1 + 0.01*norm(rng)) ) * dir - pMom;
    std::cout << "deltaPos: " << deltaPos << std::endl;
    std::cout << "deltaMom: " << deltaMom << std::endl;
    const Acts::Vector4 posTime ((pPos+deltaPos).x(), (pPos+deltaPos).y(), (pPos+deltaPos).z(), time);
    const Acts::Vector3 momentum = pMom+deltaMom;
    const Acts::Vector3 momentum_dir = momentum.normalized();
    double momentum_abs = momentum.norm();
    Acts::CurvilinearTrackParameters InitTrackParam(posTime, momentum_dir, momentum_abs, charge, std::make_optional(std::move(cov)));

    // the surface which the production point is bound to
    Acts::Vector3 center(0, 0, pPos.z());
    Acts::Vector3 normal(0, 0, 1);
    std::shared_ptr<const Acts::Surface> initSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(center, normal);
    // extrapolate the particle from production point to the first layer
    const Acts::Vector4 truthPosTime (pPos.x(), pPos.y(), pPos.z(), time);
    const Acts::Vector3 truthMomentum_dir = pMom.normalized();
    double truthMomentum_abs = pMom.norm();
    Acts::BoundTrackParameters startParameters(initSurface, geoctx, truthPosTime, truthMomentum_dir, truthMomentum_abs, charge, std::nullopt);
    auto truthParam = m_extrapolationTool->propagate(Gaudi::Hive::currentContext(), startParameters, *pSurface);
    std::cout << "truth pos on 1st layer: " << truthParam->position(geoctx) << std::endl;
    std::cout << "truth mom on 1st layer: " << truthParam->momentum() << std::endl;
    std::cout << "truth parameters on 1st layer: " << truthParam->parameters() << std::endl;


    // Call the Acts Kalman Filter
    // Prepare the output data with MultiTrajectory
    TrajectoryContainer trajectories;
    trajectories.reserve(1);

    // Construct a perigee surface as the target surface
    //auto pSurface = Acts::Surface::makeShared<Acts::PerigeeSurface>(
    //                Acts::Vector3{0., 0., 0.});

    // Set the KalmanFitter options
    std::unique_ptr<const Acts::Logger> logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::VERBOSE);
    Acts::KalmanFitterOptions<MeasurementCalibrator, Acts::VoidOutlierFinder> kfOptions(
        geoctx,
        magctx,
        calctx,
        MeasurementCalibrator(measurements),
        Acts::VoidOutlierFinder(),
        Acts::LoggerWrapper{*logger},
        Acts::PropagatorPlainOptions(),
        &(*pSurface),
        true, // scattering
        true, // energy loss
        false  // backward filtering
    );

    ATH_MSG_DEBUG("Invoke fitter");

    // Acts::Navigator     navigator(trackingGeometry);
    Acts::DirectNavigator     navigator;
    // navigator.resolvePassive   = false;
    // navigator.resolveMaterial  = true;
    // navigator.resolveSensitive = true;

    ActsExtrapolationDetail::VariantPropagator* varProp {nullptr};

    if (m_fieldMode == "FASER") {
      ATH_MSG_INFO("Using FASER magnetic field service");
      using BField_t = FASERMagneticFieldWrapper;
      BField_t bField;
      auto stepper = Acts::EigenStepper<BField_t>(std::move(bField));
      auto propagator = Acts::Propagator<decltype(stepper), Acts::DirectNavigator>(std::move(stepper),
                                                                                   std::move(navigator));
      varProp = new VariantPropagator(propagator);
    }
    else if (m_fieldMode == "Constant") {
      std::vector<double> constantFieldVector = m_constantFieldVector;
      double Bx = constantFieldVector.at(0);
      double By = constantFieldVector.at(1);
      double Bz = constantFieldVector.at(2);
      ATH_MSG_INFO("Using constant magnetic field: (Bx, By, Bz) = (" << Bx << ", " << By << ", " << Bz << ")");
      using BField_t = Acts::ConstantBField;
      BField_t bField(Bx, By, Bz);
      auto stepper = Acts::EigenStepper<BField_t>(std::move(bField));
      auto propagator = Acts::Propagator<decltype(stepper), Acts::DirectNavigator>(std::move(stepper),
                                                                                   std::move(navigator));
      varProp = new VariantPropagator(propagator);
    }

    auto fit = makeFitterFunction(varProp);
    auto result = fit(sourceLinks, InitTrackParam, kfOptions, surfSequence);

    ATH_MSG_VERBOSE("Size of sourceLinks: " << sourceLinks.size());

    int itrack = 0;
    if (result.ok()) {
      // Get the fit output object
      const auto& fitOutput = result.value();

      // The track entry indices container. One element here.
      std::vector<size_t> trackTips;
      trackTips.reserve(1);
      trackTips.emplace_back(fitOutput.trackTip);
      // The fitted parameters container. One element (at most) here.
      IndexedParams indexedParams;

      if (fitOutput.fittedParameters) {
        const auto& params = fitOutput.fittedParameters.value();
        ATH_MSG_VERBOSE("Fitted paramemeters for track " << itrack);
        ATH_MSG_VERBOSE("  parameters: " << params);
        ATH_MSG_VERBOSE("  position: " << params.position(geoctx).transpose());
        ATH_MSG_VERBOSE("  momentum: " << params.momentum().transpose());
        // Push the fitted parameters to the container
        indexedParams.emplace(fitOutput.trackTip, std::move(params));
      } else {
        ATH_MSG_DEBUG("No fitted paramemeters for track " << itrack);
      }
      // Create a SimMultiTrajectory
      trajectories.emplace_back(std::move(fitOutput.fittedStates),
                                std::move(trackTips), std::move(indexedParams));
    } else {
      ATH_MSG_WARNING("Fit failed for track " << itrack << " with error"
                                              << result.error());
      // Fit failed, but still create a empty truth fit track
      trajectories.push_back(FaserActsRecMultiTrajectory());
    }


    fillFitResult(geoctx, trajectories, *truthParam);

  }
  return StatusCode::SUCCESS;
}

StatusCode FaserActsKalmanFilterAlg::finalize()
{
  return StatusCode::SUCCESS;
}

namespace {

  template <typename Fitter>
  struct FitterFunctionImpl
  {
    Fitter fitter;

    FitterFunctionImpl(Fitter&& f) : fitter(std::move(f)) {}

    FaserActsKalmanFilterAlg::FitterResult
    operator()(
        const std::vector<IndexSourceLink>&    sourceLinks,
        const Acts::CurvilinearTrackParameters&   initialParameters,
        const Acts::KalmanFitterOptions<MeasurementCalibrator, Acts::VoidOutlierFinder>& options,
        const std::vector<const Acts::Surface*>& sSequence) const
    {
      return fitter.fit(sourceLinks, initialParameters, options, sSequence);
    };
  };
}

FaserActsKalmanFilterAlg::FitterFunction
FaserActsKalmanFilterAlg::makeFitterFunction(
    ActsExtrapolationDetail::VariantPropagator* varProp)
{

  return boost::apply_visitor([&](const auto& propagator) -> FitterFunction {
    using Updater  = Acts::GainMatrixUpdater;
    using Smoother = Acts::GainMatrixSmoother;
    using Fitter = Acts::KalmanFitter<typename std::decay_t<decltype(propagator)>, Updater, Smoother>;

    Fitter     fitter(std::move(propagator));
    // build the fitter functions. owns the fitter object.
    return FitterFunctionImpl<Fitter>(std::move(fitter));
  }, *varProp);

}

//Acts::MagneticFieldContext FaserActsKalmanFilterAlg::getMagneticFieldContext(const EventContext& ctx) const {
//SG::ReadCondHandle<FaserFieldCacheCondObj> readHandle{m_fieldCondObjInputKey, ctx};
Acts::MagneticFieldContext FaserActsKalmanFilterAlg::getMagneticFieldContext() const {
  SG::ReadCondHandle<FaserFieldCacheCondObj> readHandle{m_fieldCondObjInputKey};
  if (!readHandle.isValid()) {
    std::stringstream msg;
    msg << "Failed to retrieve magnetic field condition data " << m_fieldCondObjInputKey.key() << ".";
    throw std::runtime_error(msg.str());
  }
  const FaserFieldCacheCondObj* fieldCondObj{*readHandle};

  return Acts::MagneticFieldContext(fieldCondObj);
}

void FaserActsKalmanFilterAlg::initializeTree()
{
  m_trackTree->Branch("event_nr", &m_eventNr);
  m_trackTree->Branch("traj_nr", &m_trajNr);
  m_trackTree->Branch("track_nr", &m_trackNr);
  m_trackTree->Branch("t_barcode", &m_t_barcode, "t_barcode/l");
  m_trackTree->Branch("t_charge", &m_t_charge);
  m_trackTree->Branch("t_eT", &m_t_eT);
  m_trackTree->Branch("t_eLOC0", &m_t_eLOC0);
  m_trackTree->Branch("t_eLOC1", &m_t_eLOC1);
  m_trackTree->Branch("t_x", &m_t_x);
  m_trackTree->Branch("t_y", &m_t_y);
  m_trackTree->Branch("t_z", &m_t_z);
  m_trackTree->Branch("t_px", &m_t_px);
  m_trackTree->Branch("t_py", &m_t_py);
  m_trackTree->Branch("t_pz", &m_t_pz);
  m_trackTree->Branch("t_eTHETA", &m_t_eTHETA);
  m_trackTree->Branch("t_ePHI", &m_t_ePHI);
  m_trackTree->Branch("t_eQOP", &m_t_eQOP);

  m_trackTree->Branch("hasFittedParams", &m_hasFittedParams);
  m_trackTree->Branch("chi2_fit", &m_chi2_fit);
  m_trackTree->Branch("ndf_fit", &m_ndf_fit);
  m_trackTree->Branch("eLOC0_fit", &m_eLOC0_fit);
  m_trackTree->Branch("eLOC1_fit", &m_eLOC1_fit);
  m_trackTree->Branch("ePHI_fit", &m_ePHI_fit);
  m_trackTree->Branch("eTHETA_fit", &m_eTHETA_fit);
  m_trackTree->Branch("eQOP_fit", &m_eQOP_fit);
  m_trackTree->Branch("eT_fit", &m_eT_fit);
  m_trackTree->Branch("charge_fit", &m_charge_fit);
  m_trackTree->Branch("err_eLOC0_fit", &m_err_eLOC0_fit);
  m_trackTree->Branch("err_eLOC1_fit", &m_err_eLOC1_fit);
  m_trackTree->Branch("err_ePHI_fit", &m_err_ePHI_fit);
  m_trackTree->Branch("err_eTHETA_fit", &m_err_eTHETA_fit);
  m_trackTree->Branch("err_eQOP_fit", &m_err_eQOP_fit);
  m_trackTree->Branch("err_eT_fit", &m_err_eT_fit);
  m_trackTree->Branch("g_px_fit", &m_px_fit);
  m_trackTree->Branch("g_py_fit", &m_py_fit);
  m_trackTree->Branch("g_pz_fit", &m_pz_fit);
  m_trackTree->Branch("g_x_fit" , &m_x_fit);
  m_trackTree->Branch("g_y_fit" , &m_y_fit);
  m_trackTree->Branch("g_z_fit" , &m_z_fit);

  m_trackTree->Branch("nHoles", &m_nHoles);
  m_trackTree->Branch("nOutliers", &m_nOutliers);
  m_trackTree->Branch("nStates", &m_nStates);
  m_trackTree->Branch("nMeasurements", &m_nMeasurements);
  m_trackTree->Branch("volume_id", &m_volumeID);
  m_trackTree->Branch("layer_id", &m_layerID);
  m_trackTree->Branch("module_id", &m_moduleID);
  m_trackTree->Branch("l_x_hit", &m_lx_hit);
  m_trackTree->Branch("l_y_hit", &m_ly_hit);
  m_trackTree->Branch("g_x_hit", &m_x_hit);
  m_trackTree->Branch("g_y_hit", &m_y_hit);
  m_trackTree->Branch("g_z_hit", &m_z_hit);
  m_trackTree->Branch("res_x_hit", &m_res_x_hit);
  m_trackTree->Branch("res_y_hit", &m_res_y_hit);
  m_trackTree->Branch("err_x_hit", &m_err_x_hit);
  m_trackTree->Branch("err_y_hit", &m_err_y_hit);
  m_trackTree->Branch("pull_x_hit", &m_pull_x_hit);
  m_trackTree->Branch("pull_y_hit", &m_pull_y_hit);
  m_trackTree->Branch("dim_hit", &m_dim_hit);

  m_trackTree->Branch("nPredicted", &m_nPredicted);
  m_trackTree->Branch("predicted", &m_prt);
  m_trackTree->Branch("eLOC0_prt", &m_eLOC0_prt);
  m_trackTree->Branch("eLOC1_prt", &m_eLOC1_prt);
  m_trackTree->Branch("ePHI_prt", &m_ePHI_prt);
  m_trackTree->Branch("eTHETA_prt", &m_eTHETA_prt);
  m_trackTree->Branch("eQOP_prt", &m_eQOP_prt);
  m_trackTree->Branch("eT_prt", &m_eT_prt);
  m_trackTree->Branch("res_eLOC0_prt", &m_res_eLOC0_prt);
  m_trackTree->Branch("res_eLOC1_prt", &m_res_eLOC1_prt);
  m_trackTree->Branch("err_eLOC0_prt", &m_err_eLOC0_prt);
  m_trackTree->Branch("err_eLOC1_prt", &m_err_eLOC1_prt);
  m_trackTree->Branch("err_ePHI_prt", &m_err_ePHI_prt);
  m_trackTree->Branch("err_eTHETA_prt", &m_err_eTHETA_prt);
  m_trackTree->Branch("err_eQOP_prt", &m_err_eQOP_prt);
  m_trackTree->Branch("err_eT_prt", &m_err_eT_prt);
  m_trackTree->Branch("pull_eLOC0_prt", &m_pull_eLOC0_prt);
  m_trackTree->Branch("pull_eLOC1_prt", &m_pull_eLOC1_prt);
  m_trackTree->Branch("g_x_prt", &m_x_prt);
  m_trackTree->Branch("g_y_prt", &m_y_prt);
  m_trackTree->Branch("g_z_prt", &m_z_prt);
  m_trackTree->Branch("px_prt", &m_px_prt);
  m_trackTree->Branch("py_prt", &m_py_prt);
  m_trackTree->Branch("pz_prt", &m_pz_prt);
  m_trackTree->Branch("eta_prt", &m_eta_prt);
  m_trackTree->Branch("pT_prt", &m_pT_prt);

  m_trackTree->Branch("nFiltered", &m_nFiltered);
  m_trackTree->Branch("filtered", &m_flt);
  m_trackTree->Branch("eLOC0_flt", &m_eLOC0_flt);
  m_trackTree->Branch("eLOC1_flt", &m_eLOC1_flt);
  m_trackTree->Branch("ePHI_flt", &m_ePHI_flt);
  m_trackTree->Branch("eTHETA_flt", &m_eTHETA_flt);
  m_trackTree->Branch("eQOP_flt", &m_eQOP_flt);
  m_trackTree->Branch("eT_flt", &m_eT_flt);
  m_trackTree->Branch("res_eLOC0_flt", &m_res_eLOC0_flt);
  m_trackTree->Branch("res_eLOC1_flt", &m_res_eLOC1_flt);
  m_trackTree->Branch("err_eLOC0_flt", &m_err_eLOC0_flt);
  m_trackTree->Branch("err_eLOC1_flt", &m_err_eLOC1_flt);
  m_trackTree->Branch("err_ePHI_flt", &m_err_ePHI_flt);
  m_trackTree->Branch("err_eTHETA_flt", &m_err_eTHETA_flt);
  m_trackTree->Branch("err_eQOP_flt", &m_err_eQOP_flt);
  m_trackTree->Branch("err_eT_flt", &m_err_eT_flt);
  m_trackTree->Branch("pull_eLOC0_flt", &m_pull_eLOC0_flt);
  m_trackTree->Branch("pull_eLOC1_flt", &m_pull_eLOC1_flt);
  m_trackTree->Branch("g_x_flt", &m_x_flt);
  m_trackTree->Branch("g_y_flt", &m_y_flt);
  m_trackTree->Branch("g_z_flt", &m_z_flt);
  m_trackTree->Branch("px_flt", &m_px_flt);
  m_trackTree->Branch("py_flt", &m_py_flt);
  m_trackTree->Branch("pz_flt", &m_pz_flt);
  m_trackTree->Branch("eta_flt", &m_eta_flt);
  m_trackTree->Branch("pT_flt", &m_pT_flt);
  m_trackTree->Branch("chi2", &m_chi2);

  m_trackTree->Branch("nSmoothed", &m_nSmoothed);
  m_trackTree->Branch("smoothed", &m_smt);
  m_trackTree->Branch("eLOC0_smt", &m_eLOC0_smt);
  m_trackTree->Branch("eLOC1_smt", &m_eLOC1_smt);
  m_trackTree->Branch("ePHI_smt", &m_ePHI_smt);
  m_trackTree->Branch("eTHETA_smt", &m_eTHETA_smt);
  m_trackTree->Branch("eQOP_smt", &m_eQOP_smt);
  m_trackTree->Branch("eT_smt", &m_eT_smt);
  m_trackTree->Branch("res_eLOC0_smt", &m_res_eLOC0_smt);
  m_trackTree->Branch("res_eLOC1_smt", &m_res_eLOC1_smt);
  m_trackTree->Branch("err_eLOC0_smt", &m_err_eLOC0_smt);
  m_trackTree->Branch("err_eLOC1_smt", &m_err_eLOC1_smt);
  m_trackTree->Branch("err_ePHI_smt", &m_err_ePHI_smt);
  m_trackTree->Branch("err_eTHETA_smt", &m_err_eTHETA_smt);
  m_trackTree->Branch("err_eQOP_smt", &m_err_eQOP_smt);
  m_trackTree->Branch("err_eT_smt", &m_err_eT_smt);
  m_trackTree->Branch("pull_eLOC0_smt", &m_pull_eLOC0_smt);
  m_trackTree->Branch("pull_eLOC1_smt", &m_pull_eLOC1_smt);
  m_trackTree->Branch("g_x_smt", &m_x_smt);
  m_trackTree->Branch("g_y_smt", &m_y_smt);
  m_trackTree->Branch("g_z_smt", &m_z_smt);
  m_trackTree->Branch("px_smt", &m_px_smt);
  m_trackTree->Branch("py_smt", &m_py_smt);
  m_trackTree->Branch("pz_smt", &m_pz_smt);
  m_trackTree->Branch("eta_smt", &m_eta_smt);
  m_trackTree->Branch("pT_smt", &m_pT_smt);
}

void FaserActsKalmanFilterAlg::fillFitResult(
    const Acts::GeometryContext& geoctx,
    const TrajectoryContainer& trajectories,
    const Acts::BoundTrackParameters& truthParam
)
{
  m_t_eLOC0 = truthParam.parameters()[Acts::eBoundLoc0];
  m_t_eLOC1 = truthParam.parameters()[Acts::eBoundLoc1];
  m_t_ePHI = truthParam.parameters()[Acts::eBoundPhi];
  m_t_eTHETA = truthParam.parameters()[Acts::eBoundTheta];
  m_t_eQOP = truthParam.parameters()[Acts::eBoundQOverP];
  m_t_eT = truthParam.parameters()[Acts::eBoundTime];
  m_t_x = truthParam.position(geoctx)(0);
  m_t_y = truthParam.position(geoctx)(1);
  m_t_z = truthParam.position(geoctx)(2);
  m_t_px = truthParam.momentum()(0);
  m_t_py = truthParam.momentum()(1);
  m_t_pz = truthParam.momentum()(2);
  std::cout<<"truth global position on the first layer = "<<m_t_x<<"  "<<m_t_y<<"  "<<m_t_z<<"  "<<std::endl;
  std::cout<<"truth momentum on the first layer = "<<m_t_px<<"  "<<m_t_py<<"  "<<m_t_pz<<"  "<<std::endl;
  std::cout<<"truth local parameters on the first layer = "<<m_t_eLOC0<<"  "<<m_t_eLOC1<<"  "<<m_t_ePHI<<"  "<<m_t_eTHETA<<"  "<<m_t_eQOP<<"  "<<std::endl;

  // Loop over the trajectories
  int iTraj = 0;
  for (const auto& traj : trajectories) {
    m_trajNr = iTraj;

    // The trajectory entry indices and the multiTrajectory
    const auto& [trackTips, mj] = traj.trajectory();
    if (trackTips.empty()) {
      ATH_MSG_WARNING("Empty multiTrajectory.");
      continue;
    }

    // Get the entry index for the single trajectory
    auto& trackTip = trackTips.front();
    std::cout<<"trackTip = "<<trackTip<<std::endl;

    // Collect the trajectory summary info
    auto trajState =
        Acts::MultiTrajectoryHelpers::trajectoryState(mj, trackTip);

    m_nMeasurements = trajState.nMeasurements;
    m_nStates = trajState.nStates;
    m_nOutliers = trajState.nOutliers;
    m_nHoles = trajState.nHoles;
    m_chi2_fit = trajState.chi2Sum;
    m_ndf_fit = trajState.NDF;
    std::cout << "Track has " << trajState.nMeasurements
              << " measurements and " << trajState.nHoles
              << " holes and " << trajState.nOutliers
              << " outliers and " << trajState.nStates
              << " states " << std::endl;

    /// If it has track parameters, fill the values
    if (traj.hasTrackParameters(trackTip))
    {
      m_hasFittedParams = true;
      const auto &boundParam = traj.trackParameters(trackTip);
      const auto &parameter = boundParam.parameters();
      const auto &covariance = *boundParam.covariance();
      m_charge_fit = boundParam.charge();
      m_eLOC0_fit = parameter[Acts::eBoundLoc0];
      m_eLOC1_fit = parameter[Acts::eBoundLoc1];
      m_ePHI_fit = parameter[Acts::eBoundPhi];
      m_eTHETA_fit = parameter[Acts::eBoundTheta];
      m_eQOP_fit = parameter[Acts::eBoundQOverP];
      m_eT_fit = parameter[Acts::eBoundTime];
      m_err_eLOC0_fit =
          sqrt(covariance(Acts::eBoundLoc0, Acts::eBoundLoc0));
      m_err_eLOC1_fit =
          sqrt(covariance(Acts::eBoundLoc1, Acts::eBoundLoc1));
      m_err_ePHI_fit = sqrt(covariance(Acts::eBoundPhi, Acts::eBoundPhi));
      m_err_eTHETA_fit =
          sqrt(covariance(Acts::eBoundTheta, Acts::eBoundTheta));
      m_err_eQOP_fit = sqrt(covariance(Acts::eBoundQOverP, Acts::eBoundQOverP));
      m_err_eT_fit = sqrt(covariance(Acts::eBoundTime, Acts::eBoundTime));

      m_px_fit = boundParam.momentum()(0);
      m_py_fit = boundParam.momentum()(1);
      m_pz_fit = boundParam.momentum()(2);
      m_x_fit  = boundParam.position(geoctx)(0);
      m_y_fit  = boundParam.position(geoctx)(1);
      m_z_fit  = boundParam.position(geoctx)(2);
    }

    m_nPredicted = 0;
    m_nFiltered = 0;
    m_nSmoothed = 0;

    mj.visitBackwards(trackTip, [&](const auto &state) {
      /// Only fill the track states with non-outlier measurement
      auto typeFlags = state.typeFlags();
      if (not typeFlags.test(Acts::TrackStateFlag::MeasurementFlag))
      {
        return true;
      }

      const auto& surface = state.referenceSurface();

      /// Get the geometry ID
      auto geoID = state.referenceSurface().geometryId();
      m_volumeID.push_back(geoID.volume());
      m_layerID.push_back(geoID.layer());
      m_moduleID.push_back(geoID.sensitive());

      // expand the local measurements into the full bound space
      Acts::BoundVector meas =
          state.projector().transpose() * state.calibrated();

      // extract local and global position
      Acts::Vector2 local(meas[Acts::eBoundLoc0], meas[Acts::eBoundLoc1]);
      Acts::Vector3 mom(1, 1, 1);
      Acts::Vector3 global =
          surface.localToGlobal(geoctx, local, mom);

      // fill the measurement info
      m_lx_hit.push_back(local[Acts::ePos0]);
      m_ly_hit.push_back(local[Acts::ePos1]);
      m_x_hit.push_back(global[Acts::ePos0]);
      m_y_hit.push_back(global[Acts::ePos1]);
      m_z_hit.push_back(global[Acts::ePos2]);

      /// Get the predicted parameter for this state
      bool predicted = false;
      if (state.hasPredicted())
      {
        predicted = true;
        m_nPredicted++;
        Acts::BoundTrackParameters parameter(
            state.referenceSurface().getSharedPtr(),
            state.predicted(),
            state.predictedCovariance());
        auto covariance = state.predictedCovariance();

        /// Local hit residual info
        auto H = state.effectiveProjector();
        auto resCov = state.effectiveCalibratedCovariance() +
                      H * covariance * H.transpose();
        auto residual = state.effectiveCalibrated() - H * state.predicted();

        /// Predicted residual
        m_res_eLOC0_prt.push_back(residual(Acts::eBoundLoc0));
        m_res_eLOC1_prt.push_back(residual(Acts::eBoundLoc1));

        /// Predicted parameter pulls
        m_pull_eLOC0_prt.push_back(
            residual(Acts::eBoundLoc0) /
            sqrt(resCov(Acts::eBoundLoc0, Acts::eBoundLoc0)));
        m_pull_eLOC1_prt.push_back(
            residual(Acts::eBoundLoc1) /
            sqrt(resCov(Acts::eBoundLoc1, Acts::eBoundLoc1)));


        /// Predicted parameter
        m_eLOC0_prt.push_back(parameter.parameters()[Acts::eBoundLoc0]);
        m_eLOC1_prt.push_back(parameter.parameters()[Acts::eBoundLoc1]);
        m_ePHI_prt.push_back(parameter.parameters()[Acts::eBoundPhi]);
        m_eTHETA_prt.push_back(parameter.parameters()[Acts::eBoundTheta]);
        m_eQOP_prt.push_back(parameter.parameters()[Acts::eBoundQOverP]);
        m_eT_prt.push_back(parameter.parameters()[Acts::eBoundTime]);

        /// Predicted parameter Uncertainties
        m_err_eLOC0_prt.push_back(
            sqrt(covariance(Acts::eBoundLoc0, Acts::eBoundLoc0)));
        m_err_eLOC1_prt.push_back(
            sqrt(covariance(Acts::eBoundLoc1, Acts::eBoundLoc1)));
        m_err_ePHI_prt.push_back(
            sqrt(covariance(Acts::eBoundPhi, Acts::eBoundPhi)));
        m_err_eTHETA_prt.push_back(
            sqrt(covariance(Acts::eBoundTheta, Acts::eBoundTheta)));
        m_err_eQOP_prt.push_back(
            sqrt(covariance(Acts::eBoundQOverP, Acts::eBoundQOverP)));
        m_err_eT_prt.push_back(
            sqrt(covariance(Acts::eBoundTime, Acts::eBoundTime)));

        m_x_prt.push_back(parameter.position(geoctx).x());
        m_y_prt.push_back(parameter.position(geoctx).y());
        m_z_prt.push_back(parameter.position(geoctx).z());
        m_px_prt.push_back(parameter.momentum().x());
        m_py_prt.push_back(parameter.momentum().y());
        m_pz_prt.push_back(parameter.momentum().z());
        m_pT_prt.push_back(parameter.transverseMomentum());
        m_eta_prt.push_back(eta(parameter.position(geoctx)));
      }
      else
      {
        /// Push bad values if no predicted parameter
        m_eLOC0_prt.push_back(-9999);
        m_eLOC1_prt.push_back(-9999);
        m_ePHI_prt.push_back(-9999);
        m_eTHETA_prt.push_back(-9999);
        m_eQOP_prt.push_back(-9999);
        m_eT_prt.push_back(-9999);
        m_res_eLOC0_prt.push_back(-9999);
        m_res_eLOC1_prt.push_back(-9999);
        m_err_eLOC0_prt.push_back(-9999);
        m_err_eLOC1_prt.push_back(-9999);
        m_err_ePHI_prt.push_back(-9999);
        m_err_eTHETA_prt.push_back(-9999);
        m_err_eQOP_prt.push_back(-9999);
        m_err_eT_prt.push_back(-9999);
        m_pull_eLOC0_prt.push_back(-9999);
        m_pull_eLOC1_prt.push_back(-9999);
        m_x_prt.push_back(-9999);
        m_y_prt.push_back(-9999);
        m_z_prt.push_back(-9999);
        m_px_prt.push_back(-9999);
        m_py_prt.push_back(-9999);
        m_pz_prt.push_back(-9999);
        m_pT_prt.push_back(-9999);
        m_eta_prt.push_back(-9999);
      }

      bool filtered = false;
      if (state.hasFiltered())
      {
        filtered = true;
        m_nFiltered++;
        Acts::BoundTrackParameters parameter(
            state.referenceSurface().getSharedPtr(),
            state.filtered(),
            state.filteredCovariance());
        auto covariance = state.filteredCovariance();

        /// Local hit residual info
        auto H = state.effectiveProjector();
        auto resCov = state.effectiveCalibratedCovariance() +
                      H * covariance * H.transpose();
        auto residual = state.effectiveCalibrated() - H * state.filtered();

        /// Filtered residual
        m_res_eLOC0_flt.push_back(residual(Acts::eBoundLoc0));
        m_res_eLOC1_flt.push_back(residual(Acts::eBoundLoc1));

        /// Filtered parameter pulls
        m_pull_eLOC0_flt.push_back(
            residual(Acts::eBoundLoc0) /
            sqrt(resCov(Acts::eBoundLoc0, Acts::eBoundLoc0)));
        m_pull_eLOC1_flt.push_back(
            residual(Acts::eBoundLoc1) /
            sqrt(resCov(Acts::eBoundLoc1, Acts::eBoundLoc1)));

        /// Filtered parameter
        m_eLOC0_flt.push_back(parameter.parameters()[Acts::eBoundLoc0]);
        m_eLOC1_flt.push_back(parameter.parameters()[Acts::eBoundLoc1]);
        m_ePHI_flt.push_back(parameter.parameters()[Acts::eBoundPhi]);
        m_eTHETA_flt.push_back(parameter.parameters()[Acts::eBoundTheta]);
        m_eQOP_flt.push_back(parameter.parameters()[Acts::eBoundQOverP]);
        m_eT_flt.push_back(parameter.parameters()[Acts::eBoundTime]);

        /// Filtered parameter uncertainties
        m_err_eLOC0_flt.push_back(
            sqrt(covariance(Acts::eBoundLoc0, Acts::eBoundLoc0)));
        m_err_eLOC1_flt.push_back(
            sqrt(covariance(Acts::eBoundLoc1, Acts::eBoundLoc1)));
        m_err_ePHI_flt.push_back(
            sqrt(covariance(Acts::eBoundPhi, Acts::eBoundPhi)));
        m_err_eTHETA_flt.push_back(
            sqrt(covariance(Acts::eBoundTheta, Acts::eBoundTheta)));
        m_err_eQOP_flt.push_back(
            sqrt(covariance(Acts::eBoundQOverP, Acts::eBoundQOverP)));
        m_err_eT_flt.push_back(
            sqrt(covariance(Acts::eBoundTime, Acts::eBoundTime)));

        /// Other filtered parameter info
        m_x_flt.push_back(parameter.position(geoctx).x());
        m_y_flt.push_back(parameter.position(geoctx).y());
        m_z_flt.push_back(parameter.position(geoctx).z());
        m_px_flt.push_back(parameter.momentum().x());
        m_py_flt.push_back(parameter.momentum().y());
        m_pz_flt.push_back(parameter.momentum().z());
        m_pT_flt.push_back(parameter.transverseMomentum());
        m_eta_flt.push_back(eta(parameter.position(geoctx)));
        m_chi2.push_back(state.chi2());

      }
      else
      {
        /// Push bad values if no filtered parameter
        m_eLOC0_flt.push_back(-9999);
        m_eLOC1_flt.push_back(-9999);
        m_ePHI_flt.push_back(-9999);
        m_eTHETA_flt.push_back(-9999);
        m_eQOP_flt.push_back(-9999);
        m_eT_flt.push_back(-9999);
        m_res_eLOC0_flt.push_back(-9999);
        m_res_eLOC1_flt.push_back(-9999);
        m_err_eLOC0_flt.push_back(-9999);
        m_err_eLOC1_flt.push_back(-9999);
        m_err_ePHI_flt.push_back(-9999);
        m_err_eTHETA_flt.push_back(-9999);
        m_err_eQOP_flt.push_back(-9999);
        m_err_eT_flt.push_back(-9999);
        m_pull_eLOC0_flt.push_back(-9999);
        m_pull_eLOC1_flt.push_back(-9999);
        m_x_flt.push_back(-9999);
        m_y_flt.push_back(-9999);
        m_z_flt.push_back(-9999);
        m_py_flt.push_back(-9999);
        m_pz_flt.push_back(-9999);
        m_pT_flt.push_back(-9999);
        m_eta_flt.push_back(-9999);
        m_chi2.push_back(-9999);
      }

      bool smoothed = false;
      if (state.hasSmoothed())
      {
        smoothed = true;
        m_nSmoothed++;
        Acts::BoundTrackParameters parameter(
            state.referenceSurface().getSharedPtr(),
            state.smoothed(),
            state.smoothedCovariance());
        auto covariance = state.smoothedCovariance();

        /// Local hit residual info
        auto H = state.effectiveProjector();
        auto resCov = state.effectiveCalibratedCovariance() +
                      H * covariance * H.transpose();
        auto residual = state.effectiveCalibrated() - H * state.smoothed();

        m_res_x_hit.push_back(residual(Acts::eBoundLoc0));
        m_res_y_hit.push_back(residual(Acts::eBoundLoc1));
        m_err_x_hit.push_back(
            sqrt(resCov(Acts::eBoundLoc0, Acts::eBoundLoc0)));
        m_err_y_hit.push_back(
            sqrt(resCov(Acts::eBoundLoc1, Acts::eBoundLoc1)));
        m_pull_x_hit.push_back(
            residual(Acts::eBoundLoc0) /
            sqrt(resCov(Acts::eBoundLoc0, Acts::eBoundLoc0)));
        m_pull_y_hit.push_back(
            residual(Acts::eBoundLoc1) /
            sqrt(resCov(Acts::eBoundLoc1, Acts::eBoundLoc1)));
        m_dim_hit.push_back(state.calibratedSize());

        /// Smoothed residual
        m_res_eLOC0_smt.push_back(residual(Acts::eBoundLoc0));
        m_res_eLOC1_smt.push_back(residual(Acts::eBoundLoc1));

        /// Smoothed parameter pulls
        m_pull_eLOC0_smt.push_back(
            residual(Acts::eBoundLoc0) /
            sqrt(resCov(Acts::eBoundLoc0, Acts::eBoundLoc0)));
        m_pull_eLOC1_smt.push_back(
            residual(Acts::eBoundLoc1) /
            sqrt(resCov(Acts::eBoundLoc1, Acts::eBoundLoc1)));

        /// Smoothed parameter
        m_eLOC0_smt.push_back(parameter.parameters()[Acts::eBoundLoc0]);
        m_eLOC1_smt.push_back(parameter.parameters()[Acts::eBoundLoc1]);
        m_ePHI_smt.push_back(parameter.parameters()[Acts::eBoundPhi]);
        m_eTHETA_smt.push_back(parameter.parameters()[Acts::eBoundTheta]);
        m_eQOP_smt.push_back(parameter.parameters()[Acts::eBoundQOverP]);
        m_eT_smt.push_back(parameter.parameters()[Acts::eBoundTime]);

        /// Smoothed parameter uncertainties
        m_err_eLOC0_smt.push_back(
            sqrt(covariance(Acts::eBoundLoc0, Acts::eBoundLoc0)));
        m_err_eLOC1_smt.push_back(
            sqrt(covariance(Acts::eBoundLoc1, Acts::eBoundLoc1)));
        m_err_ePHI_smt.push_back(
            sqrt(covariance(Acts::eBoundPhi, Acts::eBoundPhi)));
        m_err_eTHETA_smt.push_back(
            sqrt(covariance(Acts::eBoundTheta, Acts::eBoundTheta)));
        m_err_eQOP_smt.push_back(
            sqrt(covariance(Acts::eBoundQOverP, Acts::eBoundQOverP)));
        m_err_eT_smt.push_back(
            sqrt(covariance(Acts::eBoundTime, Acts::eBoundTime)));

        m_x_smt.push_back(parameter.position(geoctx).x());
        m_y_smt.push_back(parameter.position(geoctx).y());
        m_z_smt.push_back(parameter.position(geoctx).z());
        m_px_smt.push_back(parameter.momentum().x());
        m_py_smt.push_back(parameter.momentum().y());
        m_pz_smt.push_back(parameter.momentum().z());
        m_pT_smt.push_back(parameter.transverseMomentum());
        m_eta_smt.push_back(eta(parameter.position(geoctx)));
      }
      else
      {
        /// Push bad values if no smoothed parameter
        m_eLOC0_smt.push_back(-9999);
        m_eLOC1_smt.push_back(-9999);
        m_ePHI_smt.push_back(-9999);
        m_eTHETA_smt.push_back(-9999);
        m_eQOP_smt.push_back(-9999);
        m_eT_smt.push_back(-9999);
        m_res_eLOC0_smt.push_back(-9999);
        m_res_eLOC1_smt.push_back(-9999);
        m_err_eLOC0_smt.push_back(-9999);
        m_err_eLOC1_smt.push_back(-9999);
        m_err_ePHI_smt.push_back(-9999);
        m_err_eTHETA_smt.push_back(-9999);
        m_err_eQOP_smt.push_back(-9999);
        m_err_eT_smt.push_back(-9999);
        m_pull_eLOC0_smt.push_back(-9999);
        m_pull_eLOC1_smt.push_back(-9999);
        m_x_smt.push_back(-9999);
        m_y_smt.push_back(-9999);
        m_z_smt.push_back(-9999);
        m_px_smt.push_back(-9999);
        m_py_smt.push_back(-9999);
        m_pz_smt.push_back(-9999);
        m_pT_smt.push_back(-9999);
        m_eta_smt.push_back(-9999);
        m_res_x_hit.push_back(-9999);
        m_res_y_hit.push_back(-9999);
        m_err_x_hit.push_back(-9999);
        m_err_y_hit.push_back(-9999);
        m_pull_x_hit.push_back(-9999);
        m_pull_y_hit.push_back(-9999);
        m_dim_hit.push_back(-9999);
      }

      /// Save whether or not states had various KF steps
      m_prt.push_back(predicted);
      m_flt.push_back(filtered);
      m_smt.push_back(smoothed);

      return true;
    }   /// Finish lambda function
    );  /// Finish multi trajectory visitBackwards call

    iTraj++;

  }  // all trajectories

  m_trackTree->Fill();

  clearTrackVariables();
}

void FaserActsKalmanFilterAlg::clearTrackVariables()
{
  m_volumeID.clear();
  m_layerID.clear();
  m_moduleID.clear();
  m_lx_hit.clear();
  m_ly_hit.clear();
  m_x_hit.clear();
  m_y_hit.clear();
  m_z_hit.clear();
  m_res_x_hit.clear();
  m_res_y_hit.clear();
  m_err_x_hit.clear();
  m_err_y_hit.clear();
  m_pull_x_hit.clear();
  m_pull_y_hit.clear();
  m_dim_hit.clear();

  m_prt.clear();
  m_eLOC0_prt.clear();
  m_eLOC1_prt.clear();
  m_ePHI_prt.clear();
  m_eTHETA_prt.clear();
  m_eQOP_prt.clear();
  m_eT_prt.clear();
  m_res_eLOC0_prt.clear();
  m_res_eLOC1_prt.clear();
  m_err_eLOC0_prt.clear();
  m_err_eLOC1_prt.clear();
  m_err_ePHI_prt.clear();
  m_err_eTHETA_prt.clear();
  m_err_eQOP_prt.clear();
  m_err_eT_prt.clear();
  m_pull_eLOC0_prt.clear();
  m_pull_eLOC1_prt.clear();
  m_x_prt.clear();
  m_y_prt.clear();
  m_z_prt.clear();
  m_px_prt.clear();
  m_py_prt.clear();
  m_pz_prt.clear();
  m_eta_prt.clear();
  m_pT_prt.clear();

  m_flt.clear();
  m_eLOC0_flt.clear();
  m_eLOC1_flt.clear();
  m_ePHI_flt.clear();
  m_eTHETA_flt.clear();
  m_eQOP_flt.clear();
  m_eT_flt.clear();
  m_res_eLOC0_flt.clear();
  m_res_eLOC1_flt.clear();
  m_err_eLOC0_flt.clear();
  m_err_eLOC1_flt.clear();
  m_err_ePHI_flt.clear();
  m_err_eTHETA_flt.clear();
  m_err_eQOP_flt.clear();
  m_err_eT_flt.clear();
  m_pull_eLOC0_flt.clear();
  m_pull_eLOC1_flt.clear();
  m_x_flt.clear();
  m_y_flt.clear();
  m_z_flt.clear();
  m_px_flt.clear();
  m_py_flt.clear();
  m_pz_flt.clear();
  m_eta_flt.clear();
  m_pT_flt.clear();
  m_chi2.clear();

  m_smt.clear();
  m_eLOC0_smt.clear();
  m_eLOC1_smt.clear();
  m_ePHI_smt.clear();
  m_eTHETA_smt.clear();
  m_eQOP_smt.clear();
  m_eT_smt.clear();
  m_res_eLOC0_smt.clear();
  m_res_eLOC1_smt.clear();
  m_err_eLOC0_smt.clear();
  m_err_eLOC1_smt.clear();
  m_err_ePHI_smt.clear();
  m_err_eTHETA_smt.clear();
  m_err_eQOP_smt.clear();
  m_err_eT_smt.clear();
  m_pull_eLOC0_smt.clear();
  m_pull_eLOC1_smt.clear();
  m_x_smt.clear();
  m_y_smt.clear();
  m_z_smt.clear();
  m_px_smt.clear();
  m_py_smt.clear();
  m_pz_smt.clear();
  m_eta_smt.clear();
  m_pT_smt.clear();

  return;
}
