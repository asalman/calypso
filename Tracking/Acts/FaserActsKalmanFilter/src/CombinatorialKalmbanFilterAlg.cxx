#include "FaserActsKalmanFilter/CombinatorialKalmanFilterAlg.h"

#include "StoreGate/ReadHandle.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "TrkSpacePoint/SpacePointCollection.h"
#include "TrkSpacePoint/SpacePoint.h"
#include "TrkPrepRawData/PrepRawData.h"
#include "TrkSurfaces/Surface.h"
#include "Identifier/Identifier.h"
#include "Acts/Geometry/GeometryIdentifier.hpp"
#include "Acts/EventData/TrackParameters.hpp"
#include "FaserActsKalmanFilter/IndexSourceLink.h"
#include "FaserActsKalmanFilter/Measurement.h"
#include "FaserActsKalmanFilter/FaserActsRecMultiTrajectory.h"
#include "Acts/Surfaces/PerigeeSurface.hpp"
#include "Acts/MagneticField/MagneticFieldContext.hpp"

using IdentifierMap = std::map<Identifier, Acts::GeometryIdentifier>;
using ThisMeasurement = Acts::Measurement<IndexSourceLink, Acts::BoundIndices, 2>;
using TrajectoriesContainer = std::vector<FaserActsRecMultiTrajectory>;
std::array<Acts::BoundIndices, 2> indices = {Acts::eBoundLoc0, Acts::eBoundLoc1};


CombinatorialKalmanFilterAlg::CombinatorialKalmanFilterAlg(
    const std::string& name, ISvcLocator* pSvcLocator)
    : AthReentrantAlgorithm(name, pSvcLocator) {}


StatusCode CombinatorialKalmanFilterAlg::initialize() {
  ATH_MSG_INFO("CombinatorialKalmanFilterAlg::initialize");

  ATH_CHECK(m_trackingGeometryTool.retrieve());
  ATH_CHECK(m_initialParameterTool.retrieve());
  ATH_CHECK(m_trajectoryWriterTool.retrieve());

  ATH_CHECK( m_fieldCondObjInputKey.initialize() );

  if (m_SpacePointContainerKey.key().empty()) {
    ATH_MSG_FATAL("empty space point container key");
    return StatusCode::FAILURE;
  }
  ATH_CHECK(m_SpacePointContainerKey.initialize());

  return StatusCode::SUCCESS;
}


StatusCode CombinatorialKalmanFilterAlg::execute(const EventContext& ctx) const {
  ATH_MSG_INFO("CombinatorialKalmanFilterAlg::execute");

  SG::ReadHandle<SpacePointContainer> spcontainer(m_SpacePointContainerKey, ctx);
  if (!spcontainer.isValid()) {
    ATH_MSG_FATAL( "Could not find the data object "<< spcontainer.name());
    return StatusCode::FAILURE;
  }

  const std::shared_ptr<IdentifierMap> identifierMap
      = m_trackingGeometryTool->getIdentifierMap();

  // Create measurement and source link containers
  IndexSourceLinkContainer sourceLinks;
  MeasurementContainer measurements;
  std::vector<Identifier> sp_ids;

  SpacePointContainer::const_iterator coll_it = spcontainer->begin();
  SpacePointContainer::const_iterator coll_itend = spcontainer->end();
  for (; coll_it != coll_itend; ++coll_it) {
    const SpacePointCollection* spcollection = *coll_it;
    SpacePointCollection::const_iterator sp_it = spcollection->begin();
    SpacePointCollection::const_iterator sp_end = spcollection->end();
    for (; sp_it != sp_end; ++sp_it) {
      const Trk::SpacePoint* sp = *sp_it;
      Identifier id = sp->associatedSurface().associatedDetectorElementIdentifier();
      Acts::GeometryIdentifier geoId = identifierMap->at(id);
      IndexSourceLink sourceLink(geoId, measurements.size());
      sourceLinks.emplace_hint(sourceLinks.end(), std::move(sourceLink));
      ThisMeasurement meas(sourceLink, indices, sp->localParameters(), sp->localCovariance());
      measurements.emplace_back(std::move(meas));

      sp_ids.push_back(sp->clusterList().first->identify());
    }
  }

  // Get initial parameters
  // FIXME: Get initial parameters from clusterFitter or SeedFinder not MC!
  std::vector<Acts::CurvilinearTrackParameters> initialParameters;
  auto initialParameter = m_initialParameterTool->getInitialParameters(sp_ids);
  initialParameters.push_back(initialParameter);

  // Prepare the output data with MultiTrajectory
  TrajectoriesContainer trajectories;
  trajectories.reserve(initialParameters.size());

  // Construct a perigee surface as the target surface
  auto pSurface = Acts::Surface::makeShared<Acts::PerigeeSurface>(
      Acts::Vector3{0., 0., 0.});

  Acts::PropagatorPlainOptions pOptions;
  pOptions.maxSteps = 10000;

  Acts::GeometryContext geoContext = m_trackingGeometryTool->getNominalGeometryContext().context();
  Acts::MagneticFieldContext magFieldContext = getMagneticFieldContext(ctx);
  Acts::CalibrationContext calibContext;
  double chi2Max = 15;
  size_t nMax = 10;
  Acts::MeasurementSelector::Config measurementSelectorCfg = {
    {Acts::GeometryIdentifier(), {chi2Max, nMax}},
  };
  std::unique_ptr<const Acts::Logger> logger
      = Acts::getDefaultLogger("CombinatorialKalmanFilter", Acts::Logging::INFO);

  // Set the CombinatorialKalmanFilter options
  CombinatorialKalmanFilterAlg::TrackFinderOptions options(
      geoContext, magFieldContext, calibContext,
      MeasurementCalibrator(measurements),
      Acts::MeasurementSelector(measurementSelectorCfg),
      Acts::LoggerWrapper{*logger}, pOptions, &(*pSurface));

  std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry
      = m_trackingGeometryTool->trackingGeometry();

  // Get track finder function
  auto trackFinderFunction = makeTrackFinderFunction(trackingGeometry);

  // Perform the track finding for all initial parameters
  ATH_MSG_DEBUG("Invoke track finding with " << initialParameters.size()
                                          << " seeds.");
  auto results = trackFinderFunction(sourceLinks, initialParameters, options);
  // Loop over the track finding results for all initial parameters
  for (std::size_t iseed = 0; iseed < initialParameters.size(); ++iseed) {
    // The result for this seed
    auto& result = results[iseed];
    if (result.ok()) {
      // Get the track finding output object
      const auto& trackFindingOutput = result.value();
      // Create a Trajectories result struct
      trajectories.emplace_back(std::move(trackFindingOutput.fittedStates),
                                std::move(trackFindingOutput.trackTips),
                                std::move(trackFindingOutput.fittedParameters));
    } else {
      ATH_MSG_WARNING("Track finding failed for seed " << iseed << " with error"
                                                       << result.error());
      // Track finding failed. Add an empty result so the output container has
      // the same number of entries as the input.
      trajectories.push_back(FaserActsRecMultiTrajectory());
    }
  }

  m_trajectoryWriterTool->writeout(trajectories, geoContext);

  return StatusCode::SUCCESS;
}


StatusCode CombinatorialKalmanFilterAlg::finalize() {
  ATH_MSG_INFO("CombinatorialKalmanFilterAlg::finalize");
  return StatusCode::SUCCESS;
}


Acts::MagneticFieldContext CombinatorialKalmanFilterAlg::getMagneticFieldContext(const EventContext& ctx) const {
  SG::ReadCondHandle<FaserFieldCacheCondObj> readHandle{m_fieldCondObjInputKey, ctx};
  if (!readHandle.isValid()) {
    std::stringstream msg;
    msg << "Failed to retrieve magnetic field condition data " << m_fieldCondObjInputKey.key() << ".";
    throw std::runtime_error(msg.str());
  }
  const FaserFieldCacheCondObj* fieldCondObj{*readHandle};

  return Acts::MagneticFieldContext(fieldCondObj);
}
