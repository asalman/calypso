#ifndef COMBINATORIALKALMANFILTERALG_H
#define COMBINATORIALKALMANFILTERALG_H

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "TrkSpacePoint/SpacePointContainer.h"
#include "FaserActsGeometryInterfaces/IFaserActsTrackingGeometryTool.h"
#include "FaserActsKalmanFilter/TruthBasedInitialParameterTool.h"
#include "Acts/TrackFinding/CombinatorialKalmanFilter.hpp"
#include "Acts/TrackFinding/MeasurementSelector.hpp"
#include "FaserActsKalmanFilter/Measurement.h"
#include "MagFieldConditions/FaserFieldCacheCondObj.h"
#include "FaserActsKalmanFilter/TrajectoryWriterTool.h"


class CombinatorialKalmanFilterAlg : public AthReentrantAlgorithm { 
 public:
  CombinatorialKalmanFilterAlg(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~CombinatorialKalmanFilterAlg() = default;

  StatusCode initialize() override;
  StatusCode execute(const EventContext& ctx) const override;
  StatusCode finalize() override;

  using TrackFinderOptions =
      Acts::CombinatorialKalmanFilterOptions<MeasurementCalibrator, Acts::MeasurementSelector>;
  using TrackFinderResult = std::vector<
      Acts::Result<Acts::CombinatorialKalmanFilterResult<IndexSourceLink>>>;
  using TrackFinderFunction = std::function<TrackFinderResult(
      const IndexSourceLinkContainer&, const std::vector<Acts::CurvilinearTrackParameters>&,
      const TrackFinderOptions&)>;

  static TrackFinderFunction makeTrackFinderFunction(
      std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry);

  Acts::MagneticFieldContext getMagneticFieldContext(const EventContext& ctx) const;


 private:
  ToolHandle<IFaserActsTrackingGeometryTool> m_trackingGeometryTool{this, "TrackingGeometryTool", "FaserActsTrackingGeometryTool"};
  ToolHandle<TruthBasedInitialParameterTool> m_initialParameterTool{this, "InitialParameterTool", "TruthBasedInitialParameterTool"};
  ToolHandle<TrajectoryWriterTool> m_trajectoryWriterTool{this, "OutputTool", "TrajectoryWriterTool"};
  SG::ReadCondHandleKey<FaserFieldCacheCondObj> m_fieldCondObjInputKey {this, "FaserFieldCacheCondObj", "fieldCondObj", "Name of the Magnetic Field conditions object key"};
  SG::ReadHandleKey<SpacePointContainer> m_SpacePointContainerKey{this, "SpacePointsSCTName", "SCT_SpacePointContainer", "SCT space point container"};
};

#endif // COMBINATORIALKALMANFILTERALG_H
