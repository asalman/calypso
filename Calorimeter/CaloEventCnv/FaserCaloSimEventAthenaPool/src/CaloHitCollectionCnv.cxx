/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "FaserCaloSimEventTPCnv/CaloHits/CaloHitCollectionCnv_p1.h"
#include "FaserCaloSimEventTPCnv/CaloHits/CaloHit_p1.h"
#include "CaloHitCollectionCnv.h"


CaloHitCollection_PERS* CaloHitCollectionCnv::createPersistent(CaloHitCollection* transCont) {
  MsgStream mlog(msgSvc(), "CaloHitCollectionConverter" );
  CaloHitCollectionCnv_PERS converter;
  CaloHitCollection_PERS *persObj = converter.createPersistent( transCont, mlog );
  return persObj;
}


CaloHitCollection* CaloHitCollectionCnv::createTransient() {
    MsgStream mlog(msgSvc(), "CaloHitCollectionConverter" );
    CaloHitCollectionCnv_p1   converter_p1;

    static const pool::Guid   p1_guid("134E8045-AB99-43EF-9AD1-324C48830B64");
    // static const pool::Guid   p1_guid("B2573A16-4B46-4E1E-98E3-F93421680779");

    CaloHitCollection       *trans_cont(0);
    if( this->compareClassGuid(p1_guid)) {
      std::unique_ptr< CaloHitCollection_p1 >   col_vect( this->poolReadObject< CaloHitCollection_p1 >() );
      trans_cont = converter_p1.createTransient( col_vect.get(), mlog );
    }  else {
      throw std::runtime_error("Unsupported persistent version of Data container");
    }
    return trans_cont;
}
