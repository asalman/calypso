################################################################################
# Package: FaserGeoEdit
################################################################################
# Author: 
# Author: 
################################################################################

# Declare the package name:
atlas_subdir( FaserGeoEditor )

# External dependencies:
find_package( Qt5 COMPONENTS Core Sql OpenGL Gui Network PrintSupport Widgets REQUIRED )

# Generate UI files automatically:
set( CMAKE_AUTOUIC TRUE )
# Generate MOC files automatically:
set( CMAKE_AUTOMOC TRUE )
# Generate resource files automatically:
set( CMAKE_AUTORCC TRUE )

# Component(s) in the package:
#atlas_add_library( FaserGeoEditor FaserGeoEditor/*.h src/*.h src/*.cxx src/*.qrc
#   PUBLIC_HEADERS FaserGeoEditor
#   PRIVATE_INCLUDE_DIRS 
#   ${CMAKE_CURRENT_BINARY_DIR}
#   LINK_LIBRARIES Qt5::Core Qt5::OpenGL Qt5::Gui Qt5::PrintSupport
#   PRIVATE_LINK_LIBRARIES PathResolver Qt5::Network )
   

atlas_add_executable( FaserGeoEditor FaserGeoEditor/*.h src/*.cxx src/*.h
#   PRIVATE_INCLUDE_DIRS ${QT5_INCLUDE_DIRS} ${CMAKE_CURRENT_BINARY_DIR}
   PRIVATE_INCLUDE_DIRS ${CMAKE_CURRENT_BINARY_DIR}
   LINK_LIBRARIES Qt5::Core Qt5::Sql Qt5::OpenGL Qt5::Gui Qt5::Widgets Qt5::PrintSupport)

# Install files from the package:
##atlas_install_scripts( share/* ) # installs into bin/
##atlas_install_runtime( share/* ) # install into share/ //TODO: check if we still need this!
   
