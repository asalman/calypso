/*
 Copyright 2021 CERN for the benefit of the FASER collaboration
*/

#ifndef SPACEPOINTCOLLECTION_P0_H
#define SPACEPOINTCOLLECTION_P0_H

#include <vector>

class SpacePointCollection_p0 {
 public:
  SpacePointCollection_p0();
  friend class SpacePointClusterCnv_p0;

  unsigned short m_idHash;
  unsigned short m_size;
};

inline
SpacePointCollection_p0::SpacePointCollection_p0() : m_idHash(0) { }

#endif  // SPACEPOINTCOLLECTION_P0_H
