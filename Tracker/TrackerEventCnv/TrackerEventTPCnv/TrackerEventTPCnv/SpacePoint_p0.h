/*
 Copyright 2021 CERN for the benefit of the FASER collaboration
*/

#ifndef SPACEPOINT_P0_H
#define SPACEPOINT_P0_H

class SpacePoint_p0 {
 public:
  SpacePoint_p0();
  friend class SpacePointCnv_p0;

 private:
  unsigned int m_idHash0;
  unsigned int m_idHash1;

  float m_pos_x;
  float m_pos_y;
  float m_pos_z;

  float m_cov00;
  float m_cov01;
  float m_cov02;
  float m_cov11;
  float m_cov12;
  float m_cov22;
};

inline
SpacePoint_p0::SpacePoint_p0() :
    m_idHash0(0),
    m_idHash1(0),
    m_pos_x(0),
    m_pos_y(0),
    m_pos_z(0),
    m_cov00(0),
    m_cov01(0),
    m_cov02(0),
    m_cov11(0),
    m_cov12(0),
    m_cov22(0)
{}

#endif  // SPACEPOINT_P0_H
