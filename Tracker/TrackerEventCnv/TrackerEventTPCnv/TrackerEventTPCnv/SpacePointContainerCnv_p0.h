/*
 Copyright 2021 CERN for the benefit of the FASER collaboration
*/

#ifndef SPACEPOINTCONTAINERCNV_P0_H
#define SPACEPOINTCONTAINERCNV_P0_H

#include "AthenaPoolCnvSvc/T_AthenaPoolTPConverter.h"

#include "TrkSpacePoint/SpacePointContainer.h"
#include "TrackerEventTPCnv/SpacePointContainer_p0.h"

class SpacePointContainerCnv_p0 : public T_AthenaPoolTPCnvBase<SpacePointContainer, SpacePointContainer_p0> {
 public:
  SpacePointContainerCnv_p0() {};

  virtual void persToTrans(const SpacePointContainer_p0* persObj,
                           SpacePointContainer* transObj,
                           MsgStream& log);

  virtual void transToPers(const SpacePointContainer* transObj, 
                           SpacePointContainer_p0* persObj, 
                           MsgStream& log);
};

#endif  // SPACEPOINTCONTAINERCNV_P0_H
