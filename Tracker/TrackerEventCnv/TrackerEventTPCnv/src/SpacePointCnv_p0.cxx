/*
 Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

#include "TrackerEventTPCnv/SpacePointCnv_p0.h"

void
SpacePointCnv_p0::persToTrans(const SpacePoint_p0* persObj, Tracker::FaserSCT_SpacePoint* transObj, MsgStream& /*log*/) {

  // TODO

}

void
SpacePointCnv_p0::transToPers(const Tracker::FaserSCT_SpacePoint* transObj, SpacePoint_p0* persObj, MsgStream& /*log*/) {

  auto idHashs = transObj->elementIdList();
  persObj->m_idHash0 = idHashs.first;
  persObj->m_idHash1 = idHashs.second;

  auto globalPosition = transObj->globalPosition();
  persObj->m_pos_x = globalPosition.x();
  persObj->m_pos_y = globalPosition.y();
  persObj->m_pos_z = globalPosition.z();

  auto cov = transObj->globCovariance();
  persObj->m_cov00 = cov(0,0);
  persObj->m_cov01 = cov(0,1);
  persObj->m_cov02 = cov(0,2);
  persObj->m_cov11 = cov(1,1);
  persObj->m_cov12 = cov(2,2);
  persObj->m_cov22 = cov(2,2);
}
