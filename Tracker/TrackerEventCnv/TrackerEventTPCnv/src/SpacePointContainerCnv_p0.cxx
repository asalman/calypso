/*
  Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

#include "TrackerEventTPCnv/SpacePointContainerCnv_p0.h"

#include "TrackerEventTPCnv/SpacePoint_p0.h"
#include "TrackerEventTPCnv/SpacePointCollection_p0.h"
#include "TrackerEventTPCnv/SpacePointCnv_p0.h"

void SpacePointContainerCnv_p0::persToTrans(const SpacePointContainer_p0* /*persObj*/, SpacePointContainer* /*transObj*/, MsgStream& /*log*/) {

  // TODO

}

void SpacePointContainerCnv_p0::transToPers(const SpacePointContainer* transObj, SpacePointContainer_p0* persObj, MsgStream& log) {
  log << MSG::INFO << "SpacePointContainerCnv_p0::transToPers()" << endmsg;

  SpacePointCnv_p0 spCnv;
  typedef SpacePointContainer TRANS;

  TRANS::const_iterator spColl;
  TRANS::const_iterator spCollEnd = transObj->end();
  unsigned int spCollId;
  unsigned int spId = 0;
  unsigned int nextSpId = 0;
  unsigned int lastIdHash = 0;

  // resize data vectors
  persObj->m_spacepoint_collections.resize(transObj->numberOfCollections());
  int n_spacepoints = 0; 
  for (spColl = transObj->begin(); spColl != spCollEnd; spColl++)  {
    n_spacepoints += (*spColl)->size();
  }
  persObj->m_spacepoints.resize(n_spacepoints);

  for (spColl = transObj->begin(), spCollId = 0; spColl != spCollEnd; ++spCollId, ++spColl)  {
    // create persitent representation of SpacePointCollections
    const SpacePointCollection& collection = **spColl;
    SpacePointCollection_p0& pcollection = persObj->m_spacepoint_collections[spCollId];
    pcollection.m_size = collection.size();
    pcollection.m_idHash = collection.identifyHash() - lastIdHash;
    lastIdHash = collection.identifyHash();

    spId = nextSpId;
    nextSpId += collection.size();

    // create persitent representation of SpacePoints
    for (std::size_t i = 0; i < collection.size(); ++i) {
      SpacePoint_p0* persSP = &(persObj->m_spacepoints[i + spId]);
      const Tracker::FaserSCT_SpacePoint* transSP = dynamic_cast<const Tracker::FaserSCT_SpacePoint*>(collection[i]);
      spCnv.transToPers(transSP, persSP, log);
    }
  }
}
