/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FASERSIHITCOLLECTIONCNV
#define FASERSIHITCOLLECTIONCNV

#include "TrackerSimEvent/FaserSiHitCollection.h"
#include "TrackerSimEventTPCnv/TrackerHits/FaserSiHitCollection_p1.h"
#include "TrackerSimEventTPCnv/TrackerHits/FaserSiHitCollectionCnv_p1.h"
#include "AthenaPoolCnvSvc/T_AthenaPoolCustomCnv.h"
// Gaudi
#include "GaudiKernel/MsgStream.h"
// typedef to the latest persistent version
typedef FaserSiHitCollection_p1     FaserSiHitCollection_PERS;
typedef FaserSiHitCollectionCnv_p1  FaserSiHitCollectionCnv_PERS;

class FaserSiHitCollectionCnv  : public T_AthenaPoolCustomCnv<FaserSiHitCollection, FaserSiHitCollection_PERS > {
  friend class CnvFactory<FaserSiHitCollectionCnv>;
public:
  FaserSiHitCollectionCnv(ISvcLocator* svcloc) :
        T_AthenaPoolCustomCnv<FaserSiHitCollection, FaserSiHitCollection_PERS >( svcloc) {}
protected:
  FaserSiHitCollection_PERS*  createPersistent(FaserSiHitCollection* transCont);
  FaserSiHitCollection*       createTransient ();
};


#endif
