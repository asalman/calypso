#ifndef TRKDETSTRIPCLUSTERIMPORTER_H
#define TRKDETSTRIPCLUSTERIMPORTER_H
//STL
#include <string>

// Base class
#include "AthenaBaseComps/AthAlgorithm.h"

#include "xAODFaserTracking/StripClusterContainer.h"

namespace TrkDet{
class StripClusterImporter : public AthAlgorithm {
public:
  /// Constructor with parameters:
  StripClusterImporter(const std::string &name,ISvcLocator *pSvcLocator);
  
  ~StripClusterImporter();
  
  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();

private:

  std::string                              m_clusCollectionName; 
  StatusCode retrieveFatrasCluster() const;

};


}

#endif // TRKDETSTRIPCLUSTERIMPORTER_H