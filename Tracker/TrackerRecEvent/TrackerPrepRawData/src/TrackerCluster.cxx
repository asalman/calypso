/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
   */

///////////////////////////////////////////////////////////////////
// TrackerCluster.cxx
//   Implementation file for class TrackerCluster
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////
// Version 1.0 15/07/2003 Veronique Boisvert
///////////////////////////////////////////////////////////////////

#include "TrackerPrepRawData/TrackerCluster.h"
#include "GaudiKernel/MsgStream.h"

namespace Tracker {
// Constructor for EF:
TrackerCluster::TrackerCluster(
    const Identifier &RDOId,
    const Amg::Vector2D& locpos, 
    const std::vector<Identifier>& rdoList, 
    const FaserSiWidth& width,
    const TrackerDD::SiDetectorElement* detEl,
    const Amg::MatrixX* locErrMat
    ) :
    PrepRawData(RDOId, locpos, rdoList, locErrMat), //call base class constructor    
  m_width(width),
  m_globalPosition{},
  m_detEl(detEl) {}

  TrackerCluster::TrackerCluster(
      const Identifier &RDOId,
      const Amg::Vector2D& locpos, 
      std::vector<Identifier>&& rdoList, 
      const FaserSiWidth& width,
      const TrackerDD::SiDetectorElement* detEl,
      std::unique_ptr<const Amg::MatrixX> locErrMat
      ) :
      PrepRawData(RDOId, locpos,
                  std::move(rdoList),
                  std::move(locErrMat)), //call base class constructor
    m_width(width),
    m_globalPosition{},
    m_detEl(detEl) {}

    // Destructor:
TrackerCluster::~TrackerCluster()
{
  // do not delete m_detEl since owned by DetectorStore
}

// Default constructor:
TrackerCluster::TrackerCluster():
  m_globalPosition{},
  m_detEl(0)
{}

//copy constructor:
TrackerCluster::TrackerCluster(const TrackerCluster& RIO):
  PrepRawData( RIO ),
  m_width( RIO.m_width ),
  m_globalPosition{},
  m_detEl( RIO.m_detEl )

{
  // copy only if it exists
  if (RIO.m_globalPosition) {
    m_globalPosition.set(std::make_unique<Amg::Vector3D>(*RIO.m_globalPosition));
  }
}

//move constructor:
TrackerCluster::TrackerCluster(TrackerCluster&& RIO):
  PrepRawData( std::move(RIO) ),
  m_width( std::move(RIO.m_width) ),
  m_globalPosition( std::move(RIO.m_globalPosition) ),
  m_detEl( RIO.m_detEl )

{
}

//assignment operator
TrackerCluster& TrackerCluster::operator=(const TrackerCluster& RIO){
  if (&RIO !=this) {
    Trk::PrepRawData::operator= (RIO);
    m_width = RIO.m_width;
    if (RIO.m_globalPosition) {
      m_globalPosition.set(std::make_unique<Amg::Vector3D>(*RIO.m_globalPosition));
    } else if (m_globalPosition) {
      m_globalPosition.release().reset();
    }
    m_detEl =  RIO.m_detEl ;
  }
  return *this;
} 

//move operator
TrackerCluster& TrackerCluster::operator=(TrackerCluster&& RIO){
  if (&RIO !=this) {
    Trk::PrepRawData::operator= (std::move(RIO));
    m_width = RIO.m_width;
    m_globalPosition = std::move(RIO.m_globalPosition);
    m_detEl =  RIO.m_detEl ;
  }
  return *this;
} 

MsgStream& TrackerCluster::dump( MsgStream&    stream) const
{
  stream << "TrackerCluster object"<<std::endl;

  // have to do a lot of annoying checking to make sure that PRD is valid. 
  {
    stream << "at global coordinates (x,y,z) = ("<<this->globalPosition().x()<<", "
      <<this->globalPosition().y()<<", "
      <<this->globalPosition().z()<<")"<<std::endl;
  }

  stream << "FaserSiWidth: " << m_width << std::endl;

  stream << "Base class (PrepRawData):" << std::endl;
  this->PrepRawData::dump(stream);

  return stream;
}

std::ostream& TrackerCluster::dump( std::ostream&    stream) const
{
  stream << "TrackerCluster object"<<std::endl;
  {
    stream << "at global coordinates (x,y,z) = ("<<this->globalPosition().x()<<", "
      <<this->globalPosition().y()<<", "
      <<this->globalPosition().z()<<")"<<std::endl;
  }

  stream << "FaserSiWidth: " << m_width << std::endl;

  stream << "Base Class (PrepRawData): " << std::endl;
  this->PrepRawData::dump(stream);

  return stream;
}


MsgStream&    operator << (MsgStream& stream,    const TrackerCluster& prd)
{
  return prd.dump(stream);
}

std::ostream& operator << (std::ostream& stream, const TrackerCluster& prd)
{
  return prd.dump(stream);
}

}
